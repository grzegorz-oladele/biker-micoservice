package pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.bikeradapters.out.Fixtures;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleEntity;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleMapper;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleRepository;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;
import pl.grzegorz.bikerdomain.biker.exception.BikerPermissionException;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.bikeradapters.out.Fixtures.BIKER_ID;

@ExtendWith(MockitoExtension.class)
class MotorcycleSetUnactiveCommandAdapterTest {

    @InjectMocks
    private MotorcycleSetUnactiveCommandAdapter motorcycleSetUnactiveCommandAdapter;
    @Mock
    private MotorcycleRepository motorcycleRepository;
    @Mock
    private MotorcycleMapper motorcycleMapper;

    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleEntity motorcycleEntity;

    @BeforeEach
    void setup() {
        motorcycleAggregate = Fixtures.motorcycleAggregate();
        motorcycleEntity = Fixtures.motorcycleEntity();
    }

    @Test
    void shouldCallSaveOnMotorcycleRepository() {
//        given
        var bikerId = BIKER_ID.toString();
        when(motorcycleMapper.toEntity(motorcycleAggregate)).thenReturn(motorcycleEntity);
//        when
        motorcycleSetUnactiveCommandAdapter.setUnactive(motorcycleAggregate, bikerId);
//        then
        verify(motorcycleRepository).save(motorcycleEntity);
    }

    @Test
    void shouldThrowExceptionWhenBikerWillNotHavePrivilegesToMotorcycle() {
//        given
        var bikerId = UUID.randomUUID().toString();
//        when + then
        assertThrows(BikerPermissionException.class,
                () -> motorcycleSetUnactiveCommandAdapter.setUnactive(motorcycleAggregate, bikerId));
    }

}