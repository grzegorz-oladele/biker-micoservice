package pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.bikeradapters.out.Fixtures;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleEntity;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleMapper;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleRepository;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MotorcycleMapEngineCommandAdapterTest {

    @InjectMocks
    private MotorcycleMapEngineCommandAdapter motorcycleMapEngineCommandAdapter;
    @Mock
    private MotorcycleMapper motorcycleMapper;
    @Mock
    private MotorcycleRepository motorcycleRepository;

    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleEntity motorcycleEntity;

    @BeforeEach
    void setup() {
        motorcycleAggregate = Fixtures.motorcycleAggregate();
        motorcycleEntity = Fixtures.motorcycleEntity();
    }

    @Test
    void shouldCallSaveMethodOnMotorcycleRepositoryInterface() {
//        given
        when(motorcycleMapper.toEntity(motorcycleAggregate)).thenReturn(motorcycleEntity);
//        when
        motorcycleMapEngineCommandAdapter.map(motorcycleAggregate);
//        then
        verify(motorcycleRepository).save(motorcycleEntity);
    }
}