package pl.grzegorz.bikeradapters.out;

import lombok.NoArgsConstructor;
import pl.grzegorz.bikeradapters.in.amqp.dto.motorcycle.MotorcycleUpdateOwnerDto;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerEntity;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleEntity;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewEntity;
import pl.grzegorz.bikeradapters.out.persistence.query.motorcycle.MotorcycleViewEntity;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase.MotorcycleOwnerUpdateCommand;
import pl.grzegorz.bikerapplication.ports.view.BikerView;
import pl.grzegorz.bikerapplication.ports.view.MotorcycleView;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;
import pl.grzegorz.bikerdomain.biker.data.motorcycle.MotorcycleClass;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor
public class Fixtures {

    public static final UUID AGGREGATE_ID = UUID.fromString("c328e967-e94b-4388-816c-cb96bf0fcae9");
    public static final UUID BIKER_ID = UUID.fromString("c328e967-e94b-4388-816c-cb96bf0fcae9");
    private static final UUID SECOND_BIKER_ID = UUID.fromString("62735389-6b89-4039-89ff-90f2e486704b");
    private static final UUID SECOND_AGGREGATE_ID = UUID.fromString("482daad0-2610-41c7-8767-4011ba82c4e9");
    public static final UUID MOTORCYCLE_ID = UUID.fromString("6daa9433-f08f-4fd5-903a-6bbc539ee462");

    public static BikerAggregate bikerAggregate() {
        return BikerAggregate.builder()
                .withId(BIKER_ID)
                .withFirstName("Andrzej")
                .withLastName("Andrzejewski")
                .withUserName("Andrzej_123")
                .withEmail("andrzej_123@pl")
                .withDateOfBirth(LocalDate.of(1989, 8, 21))
                .withIsActive(Boolean.FALSE)
                .build();
    }

    public static BikerEntity bikerEntity() {
        return BikerEntity.builder()
                .withId(BIKER_ID)
                .withFirstName("Andrzej")
                .withLastName("Andrzejewski")
                .withUserName("Andrzej_123")
                .withEmail("andrzej_123@pl")
                .withDateOfBirth(LocalDate.of(1989, 8, 21))
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withIsActive(Boolean.FALSE)
                .build();
    }

    public static BikerViewEntity bikerViewEntity() {
        return BikerViewEntity.builder()
                .withId(BIKER_ID)
                .withFirstName("Andrzej")
                .withLastName("Andrzejewski")
                .withUserName("Andrzej_123")
                .withEmail("andrzej_123@pl")
                .withDateOfBirth(LocalDate.of(1989, 8, 21))
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withIsActive(Boolean.FALSE)
                .build();
    }

    public static BikerEntity secondBikerEntity() {
        return BikerEntity.builder()
                .withId(SECOND_AGGREGATE_ID)
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withUserName("Tomasz_123")
                .withEmail("tomasz_123@pl")
                .withDateOfBirth(LocalDate.of(1992, 3, 7))
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withIsActive(Boolean.TRUE)
                .build();
    }

    private static BikerView firstBikerView() {
        return BikerView.builder()
                .withId(BIKER_ID)
                .withFirstName("Andrzej")
                .withUserName("Andrzej_123")
                .withEmail("andrzej_123@pl")
                .withDateOfBirth(LocalDate.of(1989, 8, 21))
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withIsActive(Boolean.FALSE)
                .build();
    }

    private static BikerView secondBikerView() {
        return BikerView.builder()
                .withId(SECOND_AGGREGATE_ID)
                .withFirstName("Tomasz")
                .withUserName("Tomasz_123")
                .withEmail("tomasz_123@pl")
                .withDateOfBirth(LocalDate.of(1992, 3, 7))
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withIsActive(Boolean.TRUE)
                .build();
    }

    public static MotorcycleView motorcycleView() {
        return MotorcycleView.builder()
                .withMotorcycleId(MOTORCYCLE_ID)
                .withBrand("test-brand")
                .withModel("test-model")
                .withHorsePower(Short.valueOf("200"))
                .withVintage(Short.valueOf("2021"))
                .withCapacity(Short.valueOf("1099"))
                .build();
    }

    public static MotorcycleAggregate motorcycleAggregate() {
        return MotorcycleAggregate.builder()
                .withId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("test-brand")
                .withModel("test-model")
                .withHorsePower(200)
                .withVintage(2021)
                .withCapacity(1099)
                .withIsActive(Boolean.TRUE)
                .withSerialNumber("IUHWEBIUWCNOUWEC")
                .withMotorcycleClass(MotorcycleClass.HYPERNAKED)
                .build();
    }

    public static MotorcycleEntity motorcycleEntity() {
        return MotorcycleEntity.builder()
                .withId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("test-brand")
                .withModel("test-model")
                .withHorsePower(200)
                .withVintage(2021)
                .withCapacity(1099)
                .withIsActive(Boolean.TRUE)
                .withSerialNumber("IUHWEBIUWCNOUWEC")
                .withMotorcycleClass(MotorcycleClass.HYPERNAKED)
                .build();
    }

    public static MotorcycleViewEntity motorcycleViewEntity() {
        return MotorcycleViewEntity.builder()
                .withId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("test-brand")
                .withModel("test-model")
                .withHorsePower(200)
                .withVintage(2021)
                .withCapacity(1099)
                .withIsActive(Boolean.TRUE)
                .withSerialNumber("IUHWEBIUWCNOUWEC")
                .withMotorcycleClass(MotorcycleClass.HYPERNAKED)
                .build();
    }

    public static MotorcycleUpdateOwnerDto motorcycleUpdateOwnerDto() {
        return MotorcycleUpdateOwnerDto.builder()
                .withMotorcycleId(MOTORCYCLE_ID)
                .withPreviousOwnerId(BIKER_ID)
                .withNewOwnerId(SECOND_BIKER_ID)
                .build();
    }
}