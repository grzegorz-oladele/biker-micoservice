package pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleEntity;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleMapper;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleRepository;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.bikeradapters.out.Fixtures.motorcycleAggregate;
import static pl.grzegorz.bikeradapters.out.Fixtures.motorcycleEntity;

@ExtendWith(MockitoExtension.class)
class MotorcycleOwnerUpdateCommandAdapterTest {

    @InjectMocks
    private MotorcycleOwnerUpdateCommandAdapter motorcycleOwnerUpdateCommandAdapter;
    @Mock
    private MotorcycleMapper motorcycleMapper;
    @Mock
    private MotorcycleRepository motorcycleRepository;

    private MotorcycleEntity motorcycleEntity;
    private MotorcycleAggregate motorcycleAggregate;

    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
        motorcycleEntity = motorcycleEntity();
    }

    @Test
    void shouldCallSaveMethodOnMotorcycleRepositoryInterface() {
//        given
        when(motorcycleMapper.toEntity(motorcycleAggregate)).thenReturn(motorcycleEntity);
//        when
        motorcycleOwnerUpdateCommandAdapter.updateOwner(motorcycleAggregate);
//        then
        verify(motorcycleRepository).save(motorcycleEntity);
    }
}