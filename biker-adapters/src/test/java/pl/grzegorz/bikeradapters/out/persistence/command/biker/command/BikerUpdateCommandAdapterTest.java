package pl.grzegorz.bikeradapters.out.persistence.command.biker.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.bikeradapters.out.Fixtures;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerEntity;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerMapper;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerRepository;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewEntity;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BikerUpdateCommandAdapterTest {

    @InjectMocks
    private BikerUpdateCommandAdapter bikerUpdateCommandAdapter;
    @Mock
    private BikerRepository bikerRepository;
    @Mock
    private BikerMapper bikerMapper;

    private BikerAggregate bikerAggregate;
    private BikerEntity bikerEntity;

    @BeforeEach
    void setup() {
        bikerAggregate = Fixtures.bikerAggregate();
        bikerEntity = Fixtures.bikerEntity();
    }

    @Test
    void shouldCallSaveMethodFromBikerRepositoryInterfaceAndPublishAllMethodFormBikerEventCommandPortClass() {
//        given
        when(bikerMapper.toUpdateEntity(bikerAggregate)).thenReturn(bikerEntity);
//        when
        bikerUpdateCommandAdapter.update(bikerAggregate);
//        then
        verify(bikerRepository).save(bikerEntity);
    }

}