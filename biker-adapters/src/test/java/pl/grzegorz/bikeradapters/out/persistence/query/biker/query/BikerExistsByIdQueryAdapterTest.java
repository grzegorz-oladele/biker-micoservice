package pl.grzegorz.bikeradapters.out.persistence.query.biker.query;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.bikeradapters.out.Fixtures;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerRepository;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewRepository;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BikerExistsByIdQueryAdapterTest {

    @InjectMocks
    private BikerExistsByIdQueryAdapter bikerExistsByIdQueryAdapter;
    @Mock
    private BikerViewRepository bikerRepository;

    @Test
    void shouldReturnTrueWhenBikerWillBeExistedInTheDatabase() {
//        given
        var id = Fixtures.AGGREGATE_ID;
        when(bikerRepository.existsById(id)).thenReturn(Boolean.TRUE);
//        when
        var result = bikerExistsByIdQueryAdapter.existsById(id.toString());
//        then
        assertThat(result, is(Boolean.TRUE));
    }

}