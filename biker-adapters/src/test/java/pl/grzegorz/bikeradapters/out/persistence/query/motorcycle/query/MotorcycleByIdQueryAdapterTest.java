package pl.grzegorz.bikeradapters.out.persistence.query.motorcycle.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.bikeradapters.out.persistence.query.motorcycle.MotorcycleViewEntity;
import pl.grzegorz.bikeradapters.out.persistence.query.motorcycle.MotorcycleViewMapper;
import pl.grzegorz.bikeradapters.out.persistence.query.motorcycle.MotorcycleViewRepository;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;
import pl.grzegorz.bikerdomain.biker.exception.BikerPermissionException;

import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static pl.grzegorz.bikeradapters.out.Fixtures.*;

@ExtendWith(MockitoExtension.class)
class MotorcycleByIdQueryAdapterTest {

    @InjectMocks
    private MotorcycleByIdQueryAdapter motorcycleByIdQueryAdapter;
    @Mock
    private MotorcycleViewRepository motorcycleRepository;
    @Mock
    private MotorcycleViewMapper motorcycleMapper;

    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleViewEntity motorcycleViewEntity;

    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
        motorcycleViewEntity = motorcycleViewEntity();
    }

    @Test
    void shouldReturnMotorcycleById() {
//        given
        var motorcycleId = MOTORCYCLE_ID.toString();
        var bikerId = BIKER_ID.toString();
        when(motorcycleRepository.findById(UUID.fromString(motorcycleId))).thenReturn(Optional.of(motorcycleViewEntity));
        when(motorcycleMapper.toDomain(motorcycleViewEntity)).thenReturn(motorcycleAggregate);
//        when
        MotorcycleAggregate result = motorcycleByIdQueryAdapter.getById(motorcycleId, bikerId);
//        then
        assertAll(
                () -> assertThat(result.id(), is(motorcycleViewEntity.id())),
                () -> assertThat(result.bikerId(), is(motorcycleViewEntity.bikerId())),
                () -> assertThat(result.brand(), is(motorcycleViewEntity.brand())),
                () -> assertThat(result.model(), is(motorcycleViewEntity.model())),
                () -> assertThat(result.horsePower(), is(motorcycleViewEntity.horsePower())),
                () -> assertThat(result.vintage(), is(motorcycleViewEntity.vintage())),
                () -> assertThat(result.capacity(), is(motorcycleViewEntity.capacity())),
                () -> assertThat(result.serialNumber(), is(motorcycleViewEntity.serialNumber())),
                () -> assertThat(result.isActive(), is(motorcycleViewEntity.isActive())),
                () -> assertThat(result.motorcycleClass(), is(motorcycleViewEntity.motorcycleClass()))
        );
    }

    @Test
    void shouldThrowBikerPermissionException() {
//        given
        var motorcycleId = MOTORCYCLE_ID.toString();
        var bikerId = UUID.randomUUID().toString();
        when(motorcycleRepository.findById(UUID.fromString(motorcycleId))).thenReturn(Optional.of(motorcycleViewEntity));
//        when + then
        assertThrows(BikerPermissionException.class,
                () -> motorcycleByIdQueryAdapter.getById(motorcycleId, bikerId));
    }
}