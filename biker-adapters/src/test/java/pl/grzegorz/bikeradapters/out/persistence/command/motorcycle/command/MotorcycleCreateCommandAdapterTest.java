package pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.bikeradapters.out.Fixtures;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleEntity;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleMapper;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleRepository;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MotorcycleCreateCommandAdapterTest {

    @InjectMocks
    private MotorcycleCreateCommandAdapter motorcycleCreateCommandAdapter;
    @Mock
    private MotorcycleRepository motorcycleRepository;
    @Mock
    private MotorcycleMapper motorcycleMapper;

    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleEntity motorcycleEntity;

    @BeforeEach
    void setup() {
        motorcycleAggregate = Fixtures.motorcycleAggregate();
        motorcycleEntity = Fixtures.motorcycleEntity();
    }

    @Test
    void shouldCallSaveOnMotorcycleRepository() {
//        given
        when(motorcycleMapper.toEntity(motorcycleAggregate)).thenReturn(motorcycleEntity);
//        when
        motorcycleCreateCommandAdapter.create(motorcycleAggregate);
//        then
        verify(motorcycleRepository).save(motorcycleEntity);
    }

}