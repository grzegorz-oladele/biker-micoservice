package pl.grzegorz.bikeradapters.in.amqp.impl.motorcycle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.bikeradapters.in.amqp.dto.motorcycle.MotorcycleUpdateOwnerDto;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase;

import static org.mockito.Mockito.verify;
import static pl.grzegorz.bikeradapters.out.Fixtures.motorcycleUpdateOwnerDto;

@ExtendWith(MockitoExtension.class)
class MotorcycleOwnerUpdateCommandHandlerImplTest {

    @InjectMocks
    private MotorcycleOwnerUpdateCommandHandlerImpl motorcycleOwnerUpdateCommandHandler;
    @Mock
    private MotorcycleOwnerUpdateCommandUseCase motorcycleOwnerUpdateCommandUseCase;

    private MotorcycleUpdateOwnerDto motorcycleUpdateOwnerDto;

    @BeforeEach
    void setup() {
        motorcycleUpdateOwnerDto = motorcycleUpdateOwnerDto();
    }

    @Test
    void shouldCallUpdateOnMotorcycleOwnerUpdateCommandUseCaseInterface() {
//        given
//        when
        motorcycleOwnerUpdateCommandHandler.updateOwner(motorcycleUpdateOwnerDto);
//        then
        verify(motorcycleOwnerUpdateCommandUseCase).update(motorcycleUpdateOwnerDto.motorcycleOwnerUpdateCommand());
    }
}