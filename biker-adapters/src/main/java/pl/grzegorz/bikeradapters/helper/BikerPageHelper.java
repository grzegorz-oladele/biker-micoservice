package pl.grzegorz.bikeradapters.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerEntity;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewEntity;
import pl.grzegorz.bikerapplication.resources.PageInfo;
import pl.grzegorz.bikerapplication.resources.ResourceFilter;

import java.util.Objects;

@Service
public class BikerPageHelper {

    public Pageable getPageRequest(ResourceFilter filter) {
        if (Objects.isNull(filter.page()) && Objects.isNull(filter.size())) {
            filter = ResourceFilter.empty();
        }
        return PageRequest.of(filter.page(), filter.size());
    }

    public PageInfo toPageInfo(Pageable pageable, Page<BikerViewEntity> bikersPage) {
        return PageInfo.builder()
                .withActualPage(pageable.getPageNumber())
                .withPageSize(pageable.getPageSize())
                .withTotalPages(bikersPage.getTotalPages())
                .withTotalRecordCount(bikersPage.getTotalElements())
                .build();
    }
}