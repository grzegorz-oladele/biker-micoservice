package pl.grzegorz.bikeradapters.in.amqp.handler.motorcycle;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.bikeradapters.in.amqp.dto.motorcycle.MotorcycleCreateDto;

public interface MotorcycleCreateCommandHandler {

    @RabbitListener(queues = "${spring.rabbitmq.motorcycle.create.queue}")
    void handle(MotorcycleCreateDto motorcycleCreateDto);
}