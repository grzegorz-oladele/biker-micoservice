package pl.grzegorz.bikeradapters.in.amqp.impl.biker;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.in.amqp.handler.biker.BikerCreateCommandHandler;
import pl.grzegorz.bikeradapters.in.amqp.dto.biker.BikerCreateDto;
import pl.grzegorz.bikerapplication.ports.in.biker.commands.BikerCreateCommandUseCase;

@Service
@RequiredArgsConstructor
class BikerCreateCommandHandlerImpl implements BikerCreateCommandHandler {

    private final BikerCreateCommandUseCase bikerCreateCommandUseCase;

    @Override
    public void handle(BikerCreateDto bikerCreateDto) {
        bikerCreateCommandUseCase.execute(bikerCreateDto.toCreateCommand());
    }
}