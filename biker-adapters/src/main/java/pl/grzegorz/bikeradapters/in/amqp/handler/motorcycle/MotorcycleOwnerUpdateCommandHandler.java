package pl.grzegorz.bikeradapters.in.amqp.handler.motorcycle;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.bikeradapters.in.amqp.dto.motorcycle.MotorcycleUpdateOwnerDto;

public interface MotorcycleOwnerUpdateCommandHandler {

    @RabbitListener(queues = "${spring.rabbitmq.motorcycle.update-owner.queue}")
    void updateOwner(MotorcycleUpdateOwnerDto motorcycleUpdateOwnerDto);
}