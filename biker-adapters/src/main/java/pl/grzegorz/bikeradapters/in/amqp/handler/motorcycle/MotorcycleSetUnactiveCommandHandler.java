package pl.grzegorz.bikeradapters.in.amqp.handler.motorcycle;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.bikeradapters.in.amqp.dto.motorcycle.MotorcycleSetUnactiveDto;

public interface MotorcycleSetUnactiveCommandHandler {

    @RabbitListener(queues = "${spring.rabbitmq.motorcycle.unactive.queue}")
    void handle(MotorcycleSetUnactiveDto motorcycleSetUnactiveDto);
}