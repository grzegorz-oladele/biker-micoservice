package pl.grzegorz.bikeradapters.in.amqp.dto.biker;

import java.io.Serializable;
import java.util.UUID;

public record BikerEnableDto(
        UUID id
) implements Serializable {
}