package pl.grzegorz.bikeradapters.in.amqp.handler.motorcycle;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.bikeradapters.in.amqp.dto.motorcycle.MotorcycleMapEngineDto;

public interface MotorcycleMapEngineCommandHandler {

    @RabbitListener(queues = "${spring.rabbitmq.motorcycle.map-engine.queue}")
    void handle(MotorcycleMapEngineDto motorcycleMapEngineDto);
}
