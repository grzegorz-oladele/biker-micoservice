package pl.grzegorz.bikeradapters.in.rest.controller.biker;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.bikeradapters.in.rest.api.biker.BikerByIdApi;
import pl.grzegorz.bikerapplication.ports.in.biker.query.BikerByIdQueryUseCase;
import pl.grzegorz.bikerapplication.ports.view.BikerView;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class BikerByIdController implements BikerByIdApi {

    private final BikerByIdQueryUseCase bikerByIdQueryUseCase;

    @Override
    public ResponseEntity<BikerView> getBikerById(String bikerId) {
        return ResponseEntity.status(HttpStatus.OK).body(bikerByIdQueryUseCase.execute(UUID.fromString(bikerId)));
    }
}