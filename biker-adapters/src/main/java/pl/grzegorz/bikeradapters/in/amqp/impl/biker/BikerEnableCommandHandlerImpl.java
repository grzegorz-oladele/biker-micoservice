package pl.grzegorz.bikeradapters.in.amqp.impl.biker;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.in.amqp.handler.biker.BikerEnableCommandHandler;
import pl.grzegorz.bikeradapters.in.amqp.dto.biker.BikerEnableDto;
import pl.grzegorz.bikerapplication.ports.in.biker.commands.BikerEnableCommandUseCase;

@Service
@RequiredArgsConstructor
class BikerEnableCommandHandlerImpl implements BikerEnableCommandHandler {

    private final BikerEnableCommandUseCase bikerEnableCommandUseCase;

    @Override
    public void handle(BikerEnableDto bikerEnableDto) {
        bikerEnableCommandUseCase.enable(bikerEnableDto.id().toString());
    }
}