package pl.grzegorz.bikeradapters.in.rest.api.biker;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.bikerapplication.ports.view.BikerView;
import pl.grzegorz.bikerapplication.resources.ResourceFilter;
import pl.grzegorz.bikerapplication.resources.ResultView;

@RequestMapping("/bikers")
public interface BikerPageApi {

    @GetMapping
    ResponseEntity<ResultView<BikerView>> getBikerList(ResourceFilter filter);
}