package pl.grzegorz.bikeradapters.in.amqp.dto.motorcycle;

import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleCreateCommandUseCase.MotorcycleCreateCommand;
import pl.grzegorz.bikerdomain.biker.data.motorcycle.MotorcycleClass;

import java.io.Serializable;
import java.util.UUID;

public record MotorcycleCreateDto(
        UUID id,
        UUID bikerId,
        String brand,
        String model,
        int capacity,
        int horsePower,
        int vintage,
        String serialNumber,
        String motorcycleClass
) implements Serializable {

    public MotorcycleCreateCommand toCreateCommand() {
        return MotorcycleCreateCommand.builder()
                .withId(id)
                .withBikerId(bikerId)
                .withBrand(brand)
                .withModel(model)
                .withCapacity(capacity)
                .withHorsePower(horsePower)
                .withVintage(vintage)
                .withSerialNumber(serialNumber)
                .withMotorcycleClass(MotorcycleClass.valueOf(motorcycleClass))
                .build();
    }
}