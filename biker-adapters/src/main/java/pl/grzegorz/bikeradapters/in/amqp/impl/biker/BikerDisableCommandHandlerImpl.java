package pl.grzegorz.bikeradapters.in.amqp.impl.biker;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.in.amqp.dto.biker.BikerEnableDto;
import pl.grzegorz.bikeradapters.in.amqp.handler.biker.BikerDisableCommandHandler;
import pl.grzegorz.bikerapplication.ports.in.biker.commands.BikerDisableCommandUseCase;

@Service
@RequiredArgsConstructor
class BikerDisableCommandHandlerImpl implements BikerDisableCommandHandler {

    private final BikerDisableCommandUseCase bikerDisableCommandUseCase;

    @Override
    public void handle(BikerEnableDto bikerDisableDto) {
        bikerDisableCommandUseCase.disable(bikerDisableDto.id().toString());
    }
}