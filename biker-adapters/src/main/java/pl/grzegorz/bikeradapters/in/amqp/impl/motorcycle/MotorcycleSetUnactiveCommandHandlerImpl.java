package pl.grzegorz.bikeradapters.in.amqp.impl.motorcycle;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.in.amqp.dto.motorcycle.MotorcycleSetUnactiveDto;
import pl.grzegorz.bikeradapters.in.amqp.handler.motorcycle.MotorcycleSetUnactiveCommandHandler;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleSetUnactiveCommandUseCase;

@Service
@RequiredArgsConstructor
class MotorcycleSetUnactiveCommandHandlerImpl implements MotorcycleSetUnactiveCommandHandler {

    private final MotorcycleSetUnactiveCommandUseCase motorcycleSetUnactiveCommandUseCase;

    @Override
    public void handle(MotorcycleSetUnactiveDto motorcycleSetUnactiveDto) {
        motorcycleSetUnactiveCommandUseCase.setUnactive(motorcycleSetUnactiveDto.id().toString(),
                motorcycleSetUnactiveDto.bikerId().toString());
    }
}