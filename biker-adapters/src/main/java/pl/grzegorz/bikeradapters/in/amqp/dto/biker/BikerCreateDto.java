package pl.grzegorz.bikeradapters.in.amqp.dto.biker;

import lombok.Builder;
import pl.grzegorz.bikerapplication.ports.in.biker.commands.BikerCreateCommandUseCase.BikerCreateCommand;

import java.io.Serializable;
import java.util.UUID;

@Builder(toBuilder = true, setterPrefix = "with")
public record BikerCreateDto(
        String id,
        String firstName,
        String lastName,
        String username,
        Boolean enable,
        String email,
        String phoneNumber,
        String dateOfBirth
) implements Serializable {

    public BikerCreateCommand toCreateCommand() {
        return BikerCreateCommand.builder()
                .withId(UUID.fromString(id))
                .withFirstName(firstName)
                .withLastName(lastName)
                .withUserName(username)
                .withDateOfBirth(dateOfBirth)
                .withEnable(enable)
                .withEmail(email)
                .withPhoneNumber(phoneNumber)
                .build();
    }
}