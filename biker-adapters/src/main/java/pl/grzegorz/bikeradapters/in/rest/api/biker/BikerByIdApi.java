package pl.grzegorz.bikeradapters.in.rest.api.biker;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.bikerapplication.ports.view.BikerView;

@RequestMapping("/bikers")
public interface BikerByIdApi {

    @GetMapping("/{bikerId}")
    ResponseEntity<BikerView> getBikerById(
            @PathVariable("bikerId") String bikerId);
}