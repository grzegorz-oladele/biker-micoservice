package pl.grzegorz.bikeradapters.in.amqp.handler.biker;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.bikeradapters.in.amqp.dto.biker.BikerCreateDto;

public interface BikerCreateCommandHandler {

    @RabbitListener(queues = "${spring.rabbitmq.biker.create.queue}")
    void handle(BikerCreateDto bikerCreateDto);
}
