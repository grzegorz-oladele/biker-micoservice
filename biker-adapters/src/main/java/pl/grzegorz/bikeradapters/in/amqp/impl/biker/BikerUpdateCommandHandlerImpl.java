package pl.grzegorz.bikeradapters.in.amqp.impl.biker;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.in.amqp.handler.biker.BikerUpdateCommandHandler;
import pl.grzegorz.bikeradapters.in.amqp.dto.biker.BikerUpdateDto;
import pl.grzegorz.bikerapplication.ports.in.biker.commands.BikerUpdateCommandUseCase;

@Service
@RequiredArgsConstructor
class BikerUpdateCommandHandlerImpl implements BikerUpdateCommandHandler {

    private final BikerUpdateCommandUseCase bikerUpdateCommandUseCase;

    @Override
    public void handle(BikerUpdateDto bikerUpdateDto) {
        var updateCommand = bikerUpdateDto.toUpdateCommand();
        bikerUpdateCommandUseCase.execute(updateCommand);
    }
}