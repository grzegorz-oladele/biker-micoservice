package pl.grzegorz.bikeradapters.in.rest.controller.biker;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.bikeradapters.in.rest.api.biker.BikerPageApi;
import pl.grzegorz.bikerapplication.ports.in.biker.query.BikerListQueryUseCase;
import pl.grzegorz.bikerapplication.ports.view.BikerView;
import pl.grzegorz.bikerapplication.resources.ResourceFilter;
import pl.grzegorz.bikerapplication.resources.ResultView;

@RestController
@RequiredArgsConstructor
@Slf4j
public class BikerPageController implements BikerPageApi {

    private final BikerListQueryUseCase bikerListQueryUseCase;

    @Override
    public ResponseEntity<ResultView<BikerView>> getBikerList(ResourceFilter filter) {
        return ResponseEntity.status(HttpStatus.OK).body(bikerListQueryUseCase.execute(filter));
    }
}