package pl.grzegorz.bikeradapters.in.amqp.dto.biker;

import pl.grzegorz.bikerapplication.ports.in.biker.commands.BikerUpdateCommandUseCase.BikerUpdateCommand;

import java.io.Serializable;
import java.util.UUID;

public record BikerUpdateDto(
        UUID id,
        String firstName,
        String lastName,
        String email,
        Boolean enable,
        String phoneNumber
) implements Serializable {

        public BikerUpdateCommand toUpdateCommand() {
                return BikerUpdateCommand.builder()
                        .withId(id)
                        .withFirstName(firstName)
                        .withLastName(lastName)
                        .withEmail(email)
                        .withPhoneNumber(phoneNumber)
                        .build();
        }
}
