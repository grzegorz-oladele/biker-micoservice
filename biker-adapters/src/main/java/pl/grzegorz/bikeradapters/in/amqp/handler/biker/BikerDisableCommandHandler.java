package pl.grzegorz.bikeradapters.in.amqp.handler.biker;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.bikeradapters.in.amqp.dto.biker.BikerEnableDto;

public interface BikerDisableCommandHandler {

    @RabbitListener(queues = {"${spring.rabbitmq.biker.disable.queue}"})
    void handle(BikerEnableDto bikerDisableDto);
}