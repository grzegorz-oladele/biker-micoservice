package pl.grzegorz.bikeradapters.in.amqp.impl.motorcycle;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.in.amqp.dto.motorcycle.MotorcycleUpdateOwnerDto;
import pl.grzegorz.bikeradapters.in.amqp.handler.motorcycle.MotorcycleOwnerUpdateCommandHandler;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase;

@Service
@RequiredArgsConstructor
class MotorcycleOwnerUpdateCommandHandlerImpl implements MotorcycleOwnerUpdateCommandHandler {

    private final MotorcycleOwnerUpdateCommandUseCase motorcycleOwnerUpdateCommandUseCase;

    @Override
    public void updateOwner(MotorcycleUpdateOwnerDto motorcycleUpdateOwnerDto) {
        motorcycleOwnerUpdateCommandUseCase.update(motorcycleUpdateOwnerDto.motorcycleOwnerUpdateCommand());
    }
}