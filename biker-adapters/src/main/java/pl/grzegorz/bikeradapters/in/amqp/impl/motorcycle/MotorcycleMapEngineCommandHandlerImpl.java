package pl.grzegorz.bikeradapters.in.amqp.impl.motorcycle;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.in.amqp.dto.motorcycle.MotorcycleMapEngineDto;
import pl.grzegorz.bikeradapters.in.amqp.handler.motorcycle.MotorcycleMapEngineCommandHandler;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleMapEngineCommandUseCase;

@Service
@RequiredArgsConstructor
class MotorcycleMapEngineCommandHandlerImpl implements MotorcycleMapEngineCommandHandler {

    private final MotorcycleMapEngineCommandUseCase motorcycleMapEngineCommandUseCase;

    @Override
    public void handle(MotorcycleMapEngineDto motorcycleMapEngineDto) {
        motorcycleMapEngineCommandUseCase.map(motorcycleMapEngineDto.toMapEngineCommand());
    }
}