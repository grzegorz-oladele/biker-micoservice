package pl.grzegorz.bikeradapters.in.amqp.impl.motorcycle;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.in.amqp.dto.motorcycle.MotorcycleCreateDto;
import pl.grzegorz.bikeradapters.in.amqp.handler.motorcycle.MotorcycleCreateCommandHandler;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleCreateCommandUseCase;

@Service
@RequiredArgsConstructor
class MotorcycleCreateCommandHandlerImpl implements MotorcycleCreateCommandHandler {

    private final MotorcycleCreateCommandUseCase motorcycleCreateCommandUseCase;

    @Override
    public void handle(MotorcycleCreateDto motorcycleCreateDto) {
        motorcycleCreateCommandUseCase.create(motorcycleCreateDto.toCreateCommand());
    }
}