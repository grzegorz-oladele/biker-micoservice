package pl.grzegorz.bikeradapters.in.amqp.dto.motorcycle;

import lombok.Builder;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase.MotorcycleOwnerUpdateCommand;

import java.io.Serializable;
import java.util.UUID;

@Builder(setterPrefix = "with")
public record MotorcycleUpdateOwnerDto(

        UUID motorcycleId,
        UUID previousOwnerId,
        UUID newOwnerId
) implements Serializable {

    public MotorcycleOwnerUpdateCommand motorcycleOwnerUpdateCommand() {
        return MotorcycleOwnerUpdateCommand.builder()
                .withMotorcycleId(motorcycleId)
                .withNewOwnerId(newOwnerId)
                .withOwnerTransferorId(previousOwnerId)
                .build();
    }
}