package pl.grzegorz.bikeradapters.in.amqp.dto.motorcycle;

import java.io.Serializable;
import java.util.UUID;

public record MotorcycleSetUnactiveDto(
        UUID id,
        UUID bikerId
) implements Serializable {
}
