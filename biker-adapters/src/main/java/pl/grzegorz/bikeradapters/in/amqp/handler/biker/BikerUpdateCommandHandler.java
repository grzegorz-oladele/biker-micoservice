package pl.grzegorz.bikeradapters.in.amqp.handler.biker;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.bikeradapters.in.amqp.dto.biker.BikerUpdateDto;

public interface BikerUpdateCommandHandler {

    @RabbitListener(queues = "${spring.rabbitmq.biker.update.queue}")
    void handle(BikerUpdateDto bikerUpdateDto);
}
