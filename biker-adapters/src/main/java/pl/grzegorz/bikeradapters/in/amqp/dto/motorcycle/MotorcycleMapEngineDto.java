package pl.grzegorz.bikeradapters.in.amqp.dto.motorcycle;


import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleMapEngineCommandUseCase.MotorcycleMapEngineCommand;

import java.io.Serializable;
import java.util.UUID;

public record MotorcycleMapEngineDto(
        UUID id,
        UUID bikerId,
        int capacity,
        int horsePower
) implements Serializable {

    public MotorcycleMapEngineCommand toMapEngineCommand() {
        return MotorcycleMapEngineCommand.builder()
                .withId(id)
                .withBikerId(bikerId)
                .withCapacity(capacity)
                .withHorsePower(horsePower)
                .build();
    }
}
