package pl.grzegorz.bikeradapters.in.amqp.handler.biker;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.bikeradapters.in.amqp.dto.biker.BikerEnableDto;

public interface BikerEnableCommandHandler {

    @RabbitListener(queues = {"${spring.rabbitmq.biker.enable.queue}"})
    void handle(BikerEnableDto bikerEnableDto);
}