package pl.grzegorz.bikeradapters.out.persistence.query.biker;

import org.springframework.stereotype.Component;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

@Component
public class BikerViewMapper {

    public BikerAggregate toDomain(BikerViewEntity biker) {
        return BikerAggregate.builder()
                .withId(biker.id())
                .withFirstName(biker.firstName())
                .withLastName(biker.lastName())
                .withUserName(biker.userName())
                .withEmail(biker.email())
                .withPhoneNumber(biker.phoneNumber())
                .withDateOfBirth(biker.dateOfBirth())
                .withCreatedAt(biker.createdAt())
                .withModifiedAt(biker.modifiedAt())
                .withIsActive(biker.isActive())
                .build();
    }

    public BikerAggregate toUpdateDomain(BikerViewEntity biker) {
        return BikerAggregate.builder()
                .withId(biker.id())
                .withFirstName(biker.firstName())
                .withUserName(biker.userName())
                .withEmail(biker.email())
                .withPhoneNumber(biker.phoneNumber())
                .withDateOfBirth(biker.dateOfBirth())
                .withCreatedAt(biker.createdAt())
                .withModifiedAt(biker.modifiedAt())
                .withIsActive(biker.isActive())
                .build();
    }
}