package pl.grzegorz.bikeradapters.out.persistence.command.biker.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerMapper;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerRepository;
import pl.grzegorz.bikerapplication.ports.out.biker.commands.BikerCreateCommandPort;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerCreateCommandAdapter implements BikerCreateCommandPort {

    private final BikerRepository bikerRepository;
    private final BikerMapper bikerMapper;

    @Override
    public void execute(BikerAggregate bikerAggregate) {
        var biker = bikerMapper.toEntity(bikerAggregate);
        bikerRepository.save(biker);
        log.info("Create biker using id -> [{}]", bikerAggregate.id());
    }
}