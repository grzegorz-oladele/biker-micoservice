package pl.grzegorz.bikeradapters.out.persistence.command.motorcycle;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import pl.grzegorz.bikerapplication.ports.view.MotorcycleView;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

@Component
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MotorcycleMapper {

    public MotorcycleView toView(MotorcycleEntity motorcycle) {
        return MotorcycleView.builder()
                .withMotorcycleId(motorcycle.id())
                .withBrand(motorcycle.brand())
                .withModel(motorcycle.model())
                .withCapacity(motorcycle.capacity())
                .withVintage(motorcycle.vintage())
                .withHorsePower(motorcycle.horsePower())
                .withSerialNumber(motorcycle.serialNumber())
                .build();
    }

    public MotorcycleAggregate toAggregate(MotorcycleEntity motorcycleEntity) {
        return MotorcycleAggregate.builder()
                .withId(motorcycleEntity.id())
                .withBikerId(motorcycleEntity.bikerId())
                .withBrand(motorcycleEntity.brand())
                .withModel(motorcycleEntity.model())
                .withCapacity(motorcycleEntity.capacity())
                .withHorsePower(motorcycleEntity.horsePower())
                .withVintage(motorcycleEntity.vintage())
                .withSerialNumber(motorcycleEntity.serialNumber())
                .withMotorcycleClass(motorcycleEntity.motorcycleClass())
                .withCreatedAt(motorcycleEntity.createdAt())
                .withModifiedAt(motorcycleEntity.modifiedAt())
                .withIsActive(motorcycleEntity.isActive())
                .build();
    }

    public MotorcycleEntity toEntity(MotorcycleAggregate aggregate) {
        return MotorcycleEntity.builder()
                .withId(aggregate.id())
                .withBikerId(aggregate.bikerId())
                .withBrand(aggregate.brand())
                .withModel(aggregate.model())
                .withCapacity(aggregate.capacity())
                .withHorsePower(aggregate.horsePower())
                .withVintage(aggregate.vintage())
                .withSerialNumber(aggregate.serialNumber())
                .withMotorcycleClass(aggregate.motorcycleClass())
                .withCreatedAt(aggregate.createdAt())
                .withModifiedAt(aggregate.modifiedAt())
                .withIsActive(aggregate.isActive())
                .build();
    }
}