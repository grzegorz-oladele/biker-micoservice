package pl.grzegorz.bikeradapters.out.persistence.query.motorcycle;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MotorcycleViewRepository extends JpaRepository<MotorcycleViewEntity, UUID> {

    List<MotorcycleViewEntity> findAllByBikerId(UUID bikerId);
}