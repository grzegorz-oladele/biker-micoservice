package pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleMapper;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleRepository;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.command.MotorcycleOwnerUpdateCommandPort;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleOwnerUpdateCommandAdapter implements MotorcycleOwnerUpdateCommandPort {

    private final MotorcycleMapper motorcycleMapper;
    private final MotorcycleRepository motorcycleRepository;

    @Override
    public void updateOwner(MotorcycleAggregate motorcycleAggregate) {
        var motorcycleEntity = motorcycleMapper.toEntity(motorcycleAggregate);
        motorcycleRepository.save(motorcycleEntity);
        log.info("Update owner of motorcycle. New owner id -> [{}]", motorcycleAggregate.bikerId());
    }
}