package pl.grzegorz.bikeradapters.out.persistence.query.biker.query;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewMapper;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewRepository;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerSimplifiedByIdQueryPort;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;
import pl.grzegorz.bikerdomain.biker.exception.BikerNotFoundException;
import pl.grzegorz.bikerdomain.biker.exception.messages.ExceptionMessage;

import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerSimplifiedByIdQueryAdapter implements BikerSimplifiedByIdQueryPort {

    private final BikerViewRepository bikerViewRepository;
    private final BikerViewMapper bikerViewMapper;

    @Override
    public BikerAggregate getBikerSimplifiedById(String bikerId) {
        return bikerViewRepository.findById(UUID.fromString(bikerId))
                .map(bikerViewMapper::toDomain)
                .orElseThrow(() -> new BikerNotFoundException(ExceptionMessage.BIKER_NOT_FOUND_MESSAGE.message()));
    }
}