package pl.grzegorz.bikeradapters.out.persistence.query.motorcycle;

import org.springframework.stereotype.Component;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

@Component
public class MotorcycleViewMapper {

    public MotorcycleAggregate toDomain(MotorcycleViewEntity motorcycleViewEntity) {
        return MotorcycleAggregate.builder()
                .withId(motorcycleViewEntity.id())
                .withBikerId(motorcycleViewEntity.bikerId())
                .withBrand(motorcycleViewEntity.brand())
                .withModel(motorcycleViewEntity.model())
                .withCapacity(motorcycleViewEntity.capacity())
                .withHorsePower(motorcycleViewEntity.horsePower())
                .withVintage(motorcycleViewEntity.vintage())
                .withSerialNumber(motorcycleViewEntity.serialNumber())
                .withMotorcycleClass(motorcycleViewEntity.motorcycleClass())
                .withCreatedAt(motorcycleViewEntity.createdAt())
                .withModifiedAt(motorcycleViewEntity.modifiedAt())
                .withIsActive(motorcycleViewEntity.isActive())
                .build();
    }
}