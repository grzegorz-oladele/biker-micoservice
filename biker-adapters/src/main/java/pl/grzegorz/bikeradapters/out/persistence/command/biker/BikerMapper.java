package pl.grzegorz.bikeradapters.out.persistence.command.biker;

import org.springframework.stereotype.Component;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

import java.time.LocalDateTime;

@Component
public class BikerMapper {

    public BikerEntity toEntity(BikerAggregate aggregate) {
        return BikerEntity.builder()
                .withId(aggregate.id())
                .withFirstName(aggregate.firstName())
                .withLastName(aggregate.lastName())
                .withUserName(aggregate.userName())
                .withEmail(aggregate.email())
                .withPhoneNumber(aggregate.phoneNumber())
                .withDateOfBirth(aggregate.dateOfBirth())
                .withCreatedAt(aggregate.createdAt())
                .withModifiedAt(aggregate.modifiedAt())
                .withIsActive(aggregate.isActive())
                .build();
    }

    public BikerEntity toUpdateEntity(BikerAggregate aggregate) {
        return BikerEntity.builder()
                .withId(aggregate.id())
                .withFirstName(aggregate.firstName())
                .withLastName(aggregate.lastName())
                .withUserName(aggregate.userName())
                .withEmail(aggregate.email())
                .withPhoneNumber(aggregate.phoneNumber())
                .withDateOfBirth(aggregate.dateOfBirth())
                .withCreatedAt(aggregate.createdAt())
                .withModifiedAt(LocalDateTime.now())
                .withIsActive(aggregate.isActive())
                .build();
    }
}