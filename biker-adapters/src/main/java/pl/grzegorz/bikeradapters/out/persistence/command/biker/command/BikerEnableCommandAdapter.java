package pl.grzegorz.bikeradapters.out.persistence.command.biker.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerMapper;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerRepository;
import pl.grzegorz.bikerapplication.ports.out.biker.commands.BikerEnableCommandPort;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerEnableCommandAdapter implements BikerEnableCommandPort {

    private final BikerRepository bikerRepository;
    private final BikerMapper bikerMapper;

    @Override
    public void enable(BikerAggregate bikerAggregate) {
        bikerAggregate.enable();
        var bikerEntity = bikerMapper.toEntity(bikerAggregate);
        bikerRepository.save(bikerEntity);
        log.info("Enable biker using id -> [{}]", bikerEntity.id());
    }
}