package pl.grzegorz.bikeradapters.out.persistence.query.biker.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewRepository;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerExistsByIdQueryPort;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class BikerExistsByIdQueryAdapter implements BikerExistsByIdQueryPort {

    private final BikerViewRepository bikerViewRepository;

    @Override
    public Boolean existsById(String bikerId) {
        return bikerViewRepository.existsById(UUID.fromString(bikerId));
    }
}