package pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleMapper;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleRepository;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.command.MotorcycleCreateCommandPort;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleCreateCommandAdapter implements MotorcycleCreateCommandPort {

    private final MotorcycleRepository motorcycleRepository;
    private final MotorcycleMapper motorcycleMapper;

    @Override
    public void create(MotorcycleAggregate motorcycleAggregate) {
        var motorcycleEntity = motorcycleMapper.toEntity(motorcycleAggregate);
        motorcycleRepository.save(motorcycleEntity);
        log.info("Create motorcycle using id -> [{}] and bikerId -> [{}]", motorcycleEntity.id(),
                motorcycleEntity.bikerId());
    }
}