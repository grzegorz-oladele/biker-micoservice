package pl.grzegorz.bikeradapters.out.persistence.command.biker.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerMapper;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerRepository;
import pl.grzegorz.bikerapplication.ports.out.biker.commands.BikerUpdateCommandPort;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerUpdateCommandAdapter implements BikerUpdateCommandPort {

    private final BikerRepository bikerRepository;
    private final BikerMapper bikerMapper;

    @Override
    public void update(BikerAggregate bikerAggregate) {
        var updatedBiker = bikerMapper.toUpdateEntity(bikerAggregate);
        bikerRepository.save(updatedBiker);
        log.info("Update biker using id -> [{}]", updatedBiker.id());
    }
}