package pl.grzegorz.bikeradapters.out.persistence.query.biker.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.helper.BikerPageHelper;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewMapper;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewRepository;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerPageQueryPort;
import pl.grzegorz.bikerapplication.resources.ResourceFilter;
import pl.grzegorz.bikerapplication.resources.ResultView;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

@Service
@RequiredArgsConstructor
class BikerPageQueryAdapter implements BikerPageQueryPort {

    private final BikerViewRepository bikerViewRepository;
    private final BikerViewMapper bikerViewMapper;
    private final BikerPageHelper bikerPageHelper;

    @Override
    public ResultView<BikerAggregate> getBikers(ResourceFilter filter) {
        var pageRequest = bikerPageHelper.getPageRequest(filter);
        var bikersPage = bikerViewRepository.findAll(pageRequest);
        return ResultView.<BikerAggregate>builder()
                .withPageInfo(bikerPageHelper.toPageInfo(pageRequest, bikersPage))
                .withResults(bikersPage.getContent().stream()
                        .map(bikerViewMapper::toDomain)
                        .toList())
                .build();
    }
}