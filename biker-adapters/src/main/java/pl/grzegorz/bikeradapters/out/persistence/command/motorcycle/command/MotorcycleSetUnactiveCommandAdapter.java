package pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleMapper;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleRepository;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.command.MotorcycleSetUnactiveCommandPort;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;
import pl.grzegorz.bikerdomain.biker.exception.BikerPermissionException;
import pl.grzegorz.bikerdomain.biker.exception.messages.ExceptionMessage;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleSetUnactiveCommandAdapter implements MotorcycleSetUnactiveCommandPort {

    private final MotorcycleRepository motorcycleRepository;
    private final MotorcycleMapper motorcycleMapper;

    @Override
    public void setUnactive(MotorcycleAggregate motorcycleAggregate, String bikerId) {
        validateBikerPermission(motorcycleAggregate, bikerId);
        motorcycleAggregate.setUnActive();
        var motorcycleEntity = motorcycleMapper.toEntity(motorcycleAggregate);
        motorcycleRepository.save(motorcycleEntity);
        log.info("Motorcycle using id -> [{}] set as unactive", motorcycleEntity.id());
    }

    private void validateBikerPermission(MotorcycleAggregate motorcycleAggregate, String bikerId) {
        if (!motorcycleAggregate.bikerId().toString().equals(bikerId)) {
            throw new BikerPermissionException(
                    String.format(ExceptionMessage.BIKER_PERMISSION_EXCEPTION.message(), bikerId, motorcycleAggregate.id()));
        }
    }
}