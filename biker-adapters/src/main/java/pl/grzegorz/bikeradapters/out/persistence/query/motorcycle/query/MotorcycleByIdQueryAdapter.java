package pl.grzegorz.bikeradapters.out.persistence.query.motorcycle.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.query.motorcycle.MotorcycleViewEntity;
import pl.grzegorz.bikeradapters.out.persistence.query.motorcycle.MotorcycleViewMapper;
import pl.grzegorz.bikeradapters.out.persistence.query.motorcycle.MotorcycleViewRepository;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;
import pl.grzegorz.bikerdomain.biker.exception.BikerPermissionException;
import pl.grzegorz.bikerdomain.biker.exception.MotorcycleNotFoundException;
import pl.grzegorz.bikerdomain.biker.exception.messages.ExceptionMessage;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class MotorcycleByIdQueryAdapter implements MotorcycleByIdQueryPort {

    private final MotorcycleViewRepository motorcycleViewRepository;
    private final MotorcycleViewMapper motorcycleViewMapper;

    @Override
    public MotorcycleAggregate getById(String motorcycleId, String bikerId) {
        return motorcycleViewRepository.findById(UUID.fromString(motorcycleId))
                .map(motorcycleViewEntity -> getMotorcycleAggregate(motorcycleId, bikerId, motorcycleViewEntity))
                .orElseThrow(() -> new MotorcycleNotFoundException(
                        String.format(ExceptionMessage.MOTORCYCLE_NOT_FOUND_MESSAGE.message(), motorcycleId)
                ));
    }

    private MotorcycleAggregate getMotorcycleAggregate(String motorcycleId, String bikerId,
                                                       MotorcycleViewEntity motorcycleViewEntity) {
        validateBikerPermission(motorcycleId, bikerId, motorcycleViewEntity);
        return motorcycleViewMapper.toDomain(motorcycleViewEntity);
    }

    private void validateBikerPermission(String motorcycleId, String bikerId,
                                         MotorcycleViewEntity motorcycleViewEntity) {
        if (!motorcycleViewEntity.bikerId().toString().equals(bikerId)) {
            throw new BikerPermissionException(
                    String.format(ExceptionMessage.BIKER_PERMISSION_EXCEPTION.message(), bikerId, motorcycleId));
        }
    }
}