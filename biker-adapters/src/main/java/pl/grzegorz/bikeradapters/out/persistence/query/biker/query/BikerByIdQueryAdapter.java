package pl.grzegorz.bikeradapters.out.persistence.query.biker.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewMapper;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewRepository;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;
import pl.grzegorz.bikerdomain.biker.exception.BikerNotFoundException;

import java.util.UUID;

import static pl.grzegorz.bikerdomain.biker.exception.messages.ExceptionMessage.BIKER_NOT_FOUND_MESSAGE;

@Service
@RequiredArgsConstructor
class BikerByIdQueryAdapter implements BikerByIdQueryPort {

    private final BikerViewRepository bikerViewRepository;
    private final BikerViewMapper bikerViewMapper;

    @Override
    public BikerAggregate getById(String bikerId) {
        return bikerViewRepository.findById(UUID.fromString(bikerId))
                .map(bikerViewMapper::toDomain)
                .orElseThrow(() -> new BikerNotFoundException(String.format(BIKER_NOT_FOUND_MESSAGE.message(), bikerId)));
    }
}