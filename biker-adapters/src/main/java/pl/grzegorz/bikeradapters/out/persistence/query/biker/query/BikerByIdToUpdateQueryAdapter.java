package pl.grzegorz.bikeradapters.out.persistence.query.biker.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerMapper;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerRepository;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewEntity;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewMapper;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewRepository;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerByIdToUpdateQueryPort;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;
import pl.grzegorz.bikerdomain.biker.exception.BikerNotFoundException;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class BikerByIdToUpdateQueryAdapter implements BikerByIdToUpdateQueryPort {

    private final BikerViewRepository bikerViewRepository;
    private final BikerViewMapper bikerViewMapper;

    @Override
    public BikerAggregate execute(String bikerId) {
        var bikerViewEntity = bikerViewRepository.findById(UUID.fromString(bikerId))
                .orElseThrow(() -> new BikerNotFoundException(String.format("Biker with bikerId -> [%s] not found",
                        bikerId)));
        return bikerViewMapper.toUpdateDomain(bikerViewEntity);
    }
}