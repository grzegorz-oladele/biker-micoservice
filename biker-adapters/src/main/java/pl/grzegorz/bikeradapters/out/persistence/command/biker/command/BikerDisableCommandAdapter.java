package pl.grzegorz.bikeradapters.out.persistence.command.biker.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerMapper;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerRepository;
import pl.grzegorz.bikerapplication.ports.out.biker.commands.BikerDisableCommandPort;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerDisableCommandAdapter implements BikerDisableCommandPort {

    private final BikerRepository bikerRepository;
    private final BikerMapper bikerMapper;

    @Override
    public void disable(BikerAggregate bikerAggregate) {
        bikerAggregate.disable();
        var bikerEntity = bikerMapper.toEntity(bikerAggregate);
        bikerRepository.save(bikerEntity);
        log.info("Disable biker using id -> [{}]", bikerEntity.id());
    }
}