package pl.grzegorz.bikeradapters.out.persistence.query.biker.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerMapper;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerRepository;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewMapper;
import pl.grzegorz.bikeradapters.out.persistence.query.biker.BikerViewRepository;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerByIdSimplifiedQueryPort;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;
import pl.grzegorz.bikerdomain.biker.exception.BikerNotFoundException;
import pl.grzegorz.bikerdomain.biker.exception.messages.ExceptionMessage;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class BikerByIdSimplifiedQueryAdapter implements BikerByIdSimplifiedQueryPort {

    private final BikerViewRepository bikerViewRepository;
    private final BikerViewMapper bikerViewMapper;

    @Override
    public BikerAggregate execute(String bikerById) {
        return bikerViewRepository.findById(UUID.fromString(bikerById))
                .map(bikerViewMapper::toDomain)
                .orElseThrow(() -> new BikerNotFoundException(ExceptionMessage.BIKER_NOT_FOUND_MESSAGE.message()));
    }
}