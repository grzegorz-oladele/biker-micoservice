package pl.grzegorz.bikeradapters.out.persistence.command.biker;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface BikerRepository extends JpaRepository<BikerEntity, UUID> {
}