package pl.grzegorz.bikeradapters.out.persistence.command.motorcycle;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.Accessors;
import pl.grzegorz.bikerdomain.biker.data.motorcycle.MotorcycleClass;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "motorcycles")
@Getter
@Builder(setterPrefix = "with")
@Accessors(fluent = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
public class MotorcycleEntity {

    @Id
    @Column(nullable = false, unique = true)
    private UUID id;
    private UUID bikerId;
    private String brand;
    private String model;
    private Integer capacity;
    private Integer horsePower;
    private Integer vintage;
    @Column(nullable = false, unique = true)
    private String serialNumber;
    @Enumerated(EnumType.STRING)
    private MotorcycleClass motorcycleClass;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;
    private Boolean isActive;
}