package pl.grzegorz.bikeradapters.out.persistence.query.motorcycle.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.bikeradapters.out.persistence.query.motorcycle.MotorcycleViewMapper;
import pl.grzegorz.bikeradapters.out.persistence.query.motorcycle.MotorcycleViewRepository;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.query.MotorcycleListByBikerIdQueryPort;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
class MotorcycleListByBikerIdQueryAdapter implements MotorcycleListByBikerIdQueryPort {

    private final MotorcycleViewRepository motorcycleViewRepository;
    private final MotorcycleViewMapper motorcycleViewMapper;

    @Override
    public List<MotorcycleAggregate> getMotorcycleListByBikerId(String bikerId) {
        return motorcycleViewRepository.findAllByBikerId(UUID.fromString(bikerId)).stream()
                .map(motorcycleViewMapper::toDomain)
                .collect(Collectors.toList());
    }
}