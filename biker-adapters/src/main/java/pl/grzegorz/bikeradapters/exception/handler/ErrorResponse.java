package pl.grzegorz.bikeradapters.exception.handler;

import java.time.LocalDateTime;
import lombok.AccessLevel;
import lombok.Builder;

@Builder(toBuilder = true, access = AccessLevel.PRIVATE)
record ErrorResponse(
    String message,
    String path,
    int responseCode,
    LocalDateTime timestamp
) {

  static ErrorResponse getErrorResponse(String message, String path, int responseCode) {
    return ErrorResponse.builder()
        .message(message)
        .path(path)
        .responseCode(responseCode)
        .timestamp(LocalDateTime.now())
        .build();
  }

}
