package pl.grzegorz.bikeradapters.exception.handler;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import pl.grzegorz.bikerdomain.biker.exception.BikerNotFoundException;
import pl.grzegorz.bikerdomain.biker.exception.ConnectionRefusedException;

import java.net.ConnectException;
import java.time.format.DateTimeParseException;

@RestControllerAdvice
@RequiredArgsConstructor
class GlobalExceptionHandler {

    private final HttpServletRequest request;

    @ExceptionHandler({BikerNotFoundException.class, HttpClientErrorException.NotFound.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ErrorResponse handleBikerNotFoundException(RuntimeException exception) {
        return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentUrlPath(), HttpStatus.NOT_FOUND.value());
    }

    @ExceptionHandler({ConnectionRefusedException.class})
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    ErrorResponse handleConnectException(ConnectionRefusedException exception) {
        exception.printStackTrace();
        return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentUrlPath(),
                HttpStatus.SERVICE_UNAVAILABLE.value());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
        return ErrorResponse
                .getErrorResponse(getDefaultErrorMessage(exception), getCurrentUrlPath(), HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler(ConnectException.class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    ErrorResponse handleConnectExceptionException() {
        return ErrorResponse.getErrorResponse("Connection with other microservice refused",
                getCurrentUrlPath(), HttpStatus.SERVICE_UNAVAILABLE.value());
    }

    @ExceptionHandler({DateTimeParseException.class, DataIntegrityViolationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse handleDateTimeParseException(Exception exception) {
        var errorMessage = resolveErrorMessageByException(exception);
        return ErrorResponse.getErrorResponse(errorMessage, getCurrentUrlPath(), HttpStatus.BAD_REQUEST.value());
    }

    private String resolveErrorMessageByException(Exception exception) {
        String errorMessage = null;
        if (exception instanceof DataIntegrityViolationException) {
            var exceptionMessage = exception.getMessage();
            var startPattern = "(";
            var endPattern = ") already exists.";

            int startIndex = exceptionMessage.indexOf(startPattern);
            int endIndex = exceptionMessage.indexOf(endPattern);

            if (startIndex != -1 && endIndex != -1) {
                errorMessage = String.format("Biker using %s",
                        exceptionMessage.substring(startIndex, endIndex + endPattern.length())
                                .replace("(", "")
                                .replace(")", ""));
            } else {
                errorMessage = exception.getMessage();
            }
        }
        return errorMessage;
    }

    private String getDefaultErrorMessage(MethodArgumentNotValidException exception) {
        return exception.getAllErrors().get(0).getDefaultMessage();
    }

    private String getCurrentUrlPath() {
        return request.getServletPath();
    }
}