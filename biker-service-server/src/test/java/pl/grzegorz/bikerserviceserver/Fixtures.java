package pl.grzegorz.bikerserviceserver;

import lombok.NoArgsConstructor;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerEntity;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleEntity;

import java.time.LocalDate;
import java.util.UUID;

@NoArgsConstructor
public class Fixtures {

    public static final UUID FIRST_AGGREGATE_ID = UUID.fromString("df080334-a75a-4520-aa8b-0841990f971e");
    public static final UUID SECOND_AGGREGATE_ID = UUID.fromString("8b6c47ed-a8f8-4e68-8480-6d103a925d5e");
    public static final UUID MOTORCYCLE_AGGREGATE_ID = UUID.fromString("e9412f4c-d332-4940-9719-5b100f043b15");

    public static BikerEntity getFirstBikerEntity() {
        return BikerEntity.builder()
                .withId(FIRST_AGGREGATE_ID)
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withUserName("tomek-123")
                .withEmail("tomek.123@com")
                .withDateOfBirth(LocalDate.parse("1989-04-14"))
                .withIsActive(Boolean.FALSE)
                .build();
    }

    public static BikerEntity getSecondBikerEntity() {
        return BikerEntity.builder()
                .withId(SECOND_AGGREGATE_ID)
                .withFirstName("Bartosz")
                .withLastName("Bartoszewski")
                .withUserName("bartek-123")
                .withEmail("bartek.123@com")
                .withDateOfBirth(LocalDate.parse("1987-02-03"))
                .withIsActive(Boolean.TRUE)
                .build();
    }

    public static MotorcycleEntity getMotorcycleEntity() {
        return MotorcycleEntity.builder()
                .withId(MOTORCYCLE_AGGREGATE_ID)
                .withBikerId(FIRST_AGGREGATE_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withSerialNumber("UIBQCBJNKOINQC")
                .withIsActive(Boolean.TRUE)
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2021)
                .build();
    }
}