package pl.grzegorz.bikerserviceserver.in.rest.controller.biker;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerEntity;
import pl.grzegorz.bikeradapters.out.persistence.command.biker.BikerRepository;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleEntity;
import pl.grzegorz.bikeradapters.out.persistence.command.motorcycle.MotorcycleRepository;
import pl.grzegorz.bikerserviceserver.AbstractIntegrationTest;

import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.grzegorz.bikerdomain.biker.exception.messages.ExceptionMessage.BIKER_NOT_FOUND_MESSAGE;
import static pl.grzegorz.bikerserviceserver.Fixtures.*;

class BikerControllerTest extends AbstractIntegrationTest {

    private static final String URL = "/bikers";

    @Autowired
    private BikerRepository bikerRepository;
    @Autowired
    private MotorcycleRepository motorcycleRepository;
    private BikerEntity firstBikerEntity;
    private BikerEntity secondBikerEntity;
    private MotorcycleEntity motorcycleEntity;

    @BeforeEach
    void setup() {
        firstBikerEntity = bikerRepository.save(getFirstBikerEntity());
        secondBikerEntity = bikerRepository.save(getSecondBikerEntity());
        motorcycleEntity = motorcycleRepository.save(getMotorcycleEntity());
    }

    @AfterEach
    void teardown() {
        bikerRepository.deleteAll();
        motorcycleRepository.deleteAll();
    }

    @Test
    void shouldReturnBikerViewById() throws Exception {
        mockMvc.perform(get(String.format(URL + "/%s", FIRST_AGGREGATE_ID)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is("Tomasz")))
                .andExpect(jsonPath("$.userName", is("tomek-123")))
                .andExpect(jsonPath("$.email", is("tomek.123@com")))
                .andExpect(jsonPath("$.dateOfBirth", is("1989-04-14")))
                .andExpect(jsonPath("$.motorcycles.size()", is(1)))
                .andExpect(jsonPath("$.motorcycles[0].brand", is("Ducati")))
                .andExpect(jsonPath("$.motorcycles[0].model", is("Panigale V4S")))
                .andExpect(jsonPath("$.motorcycles[0].capacity", is(1199)))
                .andExpect(jsonPath("$.motorcycles[0].horsePower", is(225)))
                .andExpect(jsonPath("$.motorcycles[0].vintage", is(2021)));
    }

    @Test
    void shouldThrowExceptionWhenBikerWillBeNotExists() throws Exception {
        var bikerId = UUID.randomUUID().toString();
        mockMvc.perform(get(String.format(URL + "/%s", bikerId)))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message", is(String.format(BIKER_NOT_FOUND_MESSAGE.message(), bikerId))))
                .andExpect(jsonPath("$.responseCode", is(404)));
    }

    @Test
    void shouldReturnPageWithBikers() throws Exception {
        mockMvc.perform(get(URL))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageInfo.pageSize", is(10)))
                .andExpect(jsonPath("$.pageInfo.actualPage", is(0)))
                .andExpect(jsonPath("$.pageInfo.totalPages", is(1)))
                .andExpect(jsonPath("$.pageInfo.totalRecordCount", is(2)))
                .andExpect(jsonPath("$.results.size()", is(2)))
                .andExpect(jsonPath("$.results[0].id", is(firstBikerEntity.id().toString())))
                .andExpect(jsonPath("$.results[0].firstName", is(firstBikerEntity.firstName())))
                .andExpect(jsonPath("$.results[0].userName", is(firstBikerEntity.userName())))
                .andExpect(jsonPath("$.results[0].email", is(firstBikerEntity.email())))
                .andExpect(jsonPath("$.results[0].phoneNumber", is(firstBikerEntity.phoneNumber())))
                .andExpect(jsonPath("$.results[0].dateOfBirth", is(firstBikerEntity.dateOfBirth().toString())))
                .andExpect(jsonPath("$.results[0].isActive", is(false)))
                .andExpect(jsonPath("$.results[0].motorcycles[0].motorcycleId", is(motorcycleEntity.id().toString())))
                .andExpect(jsonPath("$.results[0].motorcycles[0].brand", is(motorcycleEntity.brand())))
                .andExpect(jsonPath("$.results[0].motorcycles[0].model", is(motorcycleEntity.model())))
                .andExpect(jsonPath("$.results[0].motorcycles[0].capacity", is(motorcycleEntity.capacity())))
                .andExpect(jsonPath("$.results[0].motorcycles[0].horsePower", is(motorcycleEntity.horsePower())))
                .andExpect(jsonPath("$.results[0].motorcycles[0].vintage", is(motorcycleEntity.vintage())))
                .andExpect(jsonPath("$.results[1].id", is(secondBikerEntity.id().toString())))
                .andExpect(jsonPath("$.results[1].firstName", is(secondBikerEntity.firstName())))
                .andExpect(jsonPath("$.results[1].userName", is(secondBikerEntity.userName())))
                .andExpect(jsonPath("$.results[1].email", is(secondBikerEntity.email())))
                .andExpect(jsonPath("$.results[1].phoneNumber", is(secondBikerEntity.phoneNumber())))
                .andExpect(jsonPath("$.results[1].dateOfBirth", is(secondBikerEntity.dateOfBirth().toString())))
                .andExpect(jsonPath("$.results[1].isActive", is(true)));
    }
}