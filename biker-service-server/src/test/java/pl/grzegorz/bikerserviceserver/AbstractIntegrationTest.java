package pl.grzegorz.bikerserviceserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.Map;
import java.util.Set;

@ActiveProfiles("test")
@Testcontainers
@AutoConfigureMockMvc
@SpringBootTest
public abstract class AbstractIntegrationTest {

    private static final Network INTEGRATION_TEST_NETWORK = Network.newNetwork();
    private static final String POSTGRES_IMAGE = "postgres:14-alpine";
    private static final String RABBITMQ_IMAGE = "rabbitmq:3.12.0-rc.3-management-alpine";
    private static final String RABBITMQ_ALIAS = "rabbitmq";
    private static final String BIKER_DB = "biker_db";
    private static final String BIKER_DB_ALIAS = "biker-db";
    private static final String BIKER_DB_USER = "biker";
    private static final String BIKER_DB_PASSWORD = "biker";
    private static final String RABBIT_USER = "rabbit";
    private static final String RABBIT_PASSWORD = "rabbit";
    private static final int POSTGRESQL_DEFAULT_PORT = 5432;
    private static final int RABBITMQ_DEFAULT_PORT = 5672;

    private static final PostgreSQLContainer<?> BIKER_SERVICE_POSTGRE_SQL_CONTAINER = new PostgreSQLContainer<>(
            DockerImageName.parse(POSTGRES_IMAGE))
            .withDatabaseName(BIKER_DB)
            .withPassword(BIKER_DB_PASSWORD)
            .withUsername(BIKER_DB_USER)
            .withNetwork(INTEGRATION_TEST_NETWORK)
            .withNetworkAliases(BIKER_DB_ALIAS);

    private static final RabbitMQContainer RABBIT_MQ_CONTAINER = new RabbitMQContainer(
            DockerImageName.parse(RABBITMQ_IMAGE))
            .withNetwork(INTEGRATION_TEST_NETWORK)
            .withNetworkAliases(RABBITMQ_ALIAS)
            .withExposedPorts(RABBITMQ_DEFAULT_PORT)
            .withUser(RABBIT_USER, RABBIT_PASSWORD, Set.of("management"))
            .withPermission("/", RABBIT_USER, ".*", ".*", ".*")
            .withQueue("/", "events_persist", false, true, Map.of())
            .withQueue("bikers_persist", false, true, Map.of())
            .withQueue("bikers_update", false, true, Map.of())
            .withQueue("bikers_enable", false, true, Map.of())
            .withQueue("bikers_disable", false, true, Map.of())
            .withQueue("bikers_remove", false, true, Map.of())
            .withQueue("motorcycles_biker_persist", false, true, Map.of())
            .withQueue("motorcycles_biker_unactive", false, true, Map.of())
            .withQueue("motorcycles_biker_update", false, true, Map.of())
            .withQueue("motorcycles_biker_map_engine", false, true, Map.of())
            .withQueue("motorcycles_biker_update-owner", false, true, Map.of());


    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper mapper;

    static  {
        BIKER_SERVICE_POSTGRE_SQL_CONTAINER.start();
        RABBIT_MQ_CONTAINER.start();
        System.setProperty("spring.rabbitmq.port",
                String.valueOf(RABBIT_MQ_CONTAINER.getMappedPort(RABBITMQ_DEFAULT_PORT)));
    }

    @DynamicPropertySource
    public static void postgresContainerConfig(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", BIKER_SERVICE_POSTGRE_SQL_CONTAINER::getJdbcUrl);
        registry.add("spring.datasource.password", BIKER_SERVICE_POSTGRE_SQL_CONTAINER::getPassword);
        registry.add("spring.datasource.username", BIKER_SERVICE_POSTGRE_SQL_CONTAINER::getUsername);
    }
}