package pl.grzegorz.bikerserviceserver.config;

import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.grzegorz.bikerapplication.ports.in.biker.commands.BikerCreateCommandUseCase;
import pl.grzegorz.bikerapplication.ports.in.biker.commands.BikerDisableCommandUseCase;
import pl.grzegorz.bikerapplication.ports.in.biker.commands.BikerEnableCommandUseCase;
import pl.grzegorz.bikerapplication.ports.in.biker.commands.BikerUpdateCommandUseCase;
import pl.grzegorz.bikerapplication.ports.in.biker.query.BikerByIdQueryUseCase;
import pl.grzegorz.bikerapplication.ports.in.biker.query.BikerListQueryUseCase;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleCreateCommandUseCase;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleMapEngineCommandUseCase;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleSetUnactiveCommandUseCase;
import pl.grzegorz.bikerapplication.ports.out.biker.commands.BikerCreateCommandPort;
import pl.grzegorz.bikerapplication.ports.out.biker.commands.BikerDisableCommandPort;
import pl.grzegorz.bikerapplication.ports.out.biker.commands.BikerEnableCommandPort;
import pl.grzegorz.bikerapplication.ports.out.biker.commands.BikerUpdateCommandPort;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerByIdSimplifiedQueryPort;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerByIdToUpdateQueryPort;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerPageQueryPort;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.command.MotorcycleCreateCommandPort;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.command.MotorcycleMapEngineCommandPort;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.command.MotorcycleOwnerUpdateCommandPort;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.command.MotorcycleSetUnactiveCommandPort;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.query.MotorcycleListByBikerIdQueryPort;
import pl.grzegorz.bikerapplication.services.biker.command.BikerCreateCommandService;
import pl.grzegorz.bikerapplication.services.biker.command.BikerDisableCommandService;
import pl.grzegorz.bikerapplication.services.biker.command.BikerEnableCommandService;
import pl.grzegorz.bikerapplication.services.biker.command.BikerUpdateCommandService;
import pl.grzegorz.bikerapplication.services.biker.query.BikerByIdQueryService;
import pl.grzegorz.bikerapplication.services.biker.query.BikerListQueryService;
import pl.grzegorz.bikerapplication.services.motorcycle.command.MotorcycleCreateCommandService;
import pl.grzegorz.bikerapplication.services.motorcycle.command.MotorcycleMapEngineCommandService;
import pl.grzegorz.bikerapplication.services.motorcycle.command.MotorcycleOwnerUpdateCommandService;
import pl.grzegorz.bikerapplication.services.motorcycle.command.MotorcycleSetUnactiveCommandService;

@Configuration
public class BikerBeans {

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory() {
        return new SimpleRabbitListenerContainerFactory();
    }

    @Bean
    public BikerCreateCommandUseCase bikerCreateUseCase(BikerCreateCommandPort bikerCreateCommandPort) {
        return new BikerCreateCommandService(bikerCreateCommandPort);
    }

    @Bean
    public BikerByIdQueryUseCase bikerQueryUseCase(BikerByIdQueryPort bikerByIdQueryPort,
                                                   MotorcycleListByBikerIdQueryPort motorcycleListByBikerIdQueryPort) {
        return new BikerByIdQueryService(bikerByIdQueryPort, motorcycleListByBikerIdQueryPort);
    }

    @Bean
    public BikerUpdateCommandUseCase bikerUpdateUseCase(BikerUpdateCommandPort bikerUpdateCommandPort,
                                                        BikerByIdToUpdateQueryPort bikerByIdToUpdateQueryPort) {
        return new BikerUpdateCommandService(bikerUpdateCommandPort, bikerByIdToUpdateQueryPort);
    }

    @Bean
    public BikerListQueryUseCase bikerListQueryUseCase(BikerPageQueryPort bikerPageQueryPort,
                                                       MotorcycleListByBikerIdQueryPort motorcycleListByBikerIdQueryPort) {
        return new BikerListQueryService(bikerPageQueryPort, motorcycleListByBikerIdQueryPort);
    }

    @Bean
    public BikerEnableCommandUseCase bikerEnableCommandUseCase(BikerByIdSimplifiedQueryPort bikerByIdSimplifiedQueryPort,
                                                               BikerEnableCommandPort bikerEnableCommandPort) {
        return new BikerEnableCommandService(bikerByIdSimplifiedQueryPort, bikerEnableCommandPort);
    }

    @Bean
    public BikerDisableCommandUseCase bikerDisableCommandUseCase(BikerByIdSimplifiedQueryPort bikerByIdSimplifiedQueryPort,
                                                                 BikerDisableCommandPort bikerDisableCommandPort) {
        return new BikerDisableCommandService(bikerByIdSimplifiedQueryPort, bikerDisableCommandPort);
    }

    @Bean
    public MotorcycleCreateCommandUseCase motorcycleCreateCommandUseCase(MotorcycleCreateCommandPort motorcycleCreateCommandPort) {
        return new MotorcycleCreateCommandService(motorcycleCreateCommandPort);
    }

    @Bean
    public MotorcycleSetUnactiveCommandUseCase motorcycleSetUnactiveCommandUseCase(MotorcycleByIdQueryPort motorcycleByIdQueryPort,
                                                                                   MotorcycleSetUnactiveCommandPort
                                                                                           motorcycleSetUnactiveCommandPort) {
        return new MotorcycleSetUnactiveCommandService(motorcycleByIdQueryPort, motorcycleSetUnactiveCommandPort);
    }

    @Bean
    public MotorcycleMapEngineCommandUseCase motorcycleMapEngineCommandUseCase(MotorcycleByIdQueryPort motorcycleByIdQueryPort,
                                                                               MotorcycleMapEngineCommandPort motorcycleMapEngineCommandPort) {
        return new MotorcycleMapEngineCommandService(motorcycleByIdQueryPort, motorcycleMapEngineCommandPort);
    }

    @Bean
    public MotorcycleOwnerUpdateCommandUseCase motorcycleOwnerUpdateCommandUseCase(MotorcycleByIdQueryPort motorcycleByIdQueryPort,
                                                                                   MotorcycleOwnerUpdateCommandPort motorcycleOwnerUpdateCommandPort) {
        return new MotorcycleOwnerUpdateCommandService(motorcycleByIdQueryPort, motorcycleOwnerUpdateCommandPort);
    }
}