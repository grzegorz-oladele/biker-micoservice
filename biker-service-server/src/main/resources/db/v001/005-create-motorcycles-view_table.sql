CREATE OR REPLACE VIEW biker_db.bikers.motorcycles_view AS
SELECT id,
       biker_id,
       brand,
       model,
       capacity,
       horse_power,
       vintage,
       serial_number,
       motorcycle_class,
       created_at,
       modified_at,
       is_active
FROM biker_db.bikers.motorcycles;