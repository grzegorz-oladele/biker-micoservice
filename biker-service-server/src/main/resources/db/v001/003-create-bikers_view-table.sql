CREATE OR REPLACE VIEW biker_db.bikers.bikers_view AS
SELECT id,
       first_name,
       last_name,
       user_name,
       email,
       phone_number,
       date_of_birth,
       created_at,
       modified_at,
       is_active
FROM biker_db.bikers.bikers;