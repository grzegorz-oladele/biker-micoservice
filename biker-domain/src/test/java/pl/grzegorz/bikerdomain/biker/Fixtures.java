package pl.grzegorz.bikerdomain.biker;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;
import pl.grzegorz.bikerdomain.biker.data.biker.BikerCreateData;
import pl.grzegorz.bikerdomain.biker.data.biker.BikerUpdateData;
import pl.grzegorz.bikerdomain.biker.data.motorcycle.MotorcycleClass;
import pl.grzegorz.bikerdomain.biker.data.motorcycle.MotorcycleCreateData;
import pl.grzegorz.bikerdomain.biker.data.motorcycle.MotorcycleMapEngineData;
import pl.grzegorz.bikerdomain.biker.data.motorcycle.MotorcycleUpdateOwnerData;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Fixtures {

    private static final UUID BIKER_ID = UUID.fromString("c90fd34d-1ae5-4ae4-8ccd-2eadf7e01fcd");
    private static final UUID MOTORCYCLE_ID = UUID.fromString("6044a914-bf11-4cc4-bc81-6f74d5840db5");
    private static final UUID RECIPIENT_ID = UUID.fromString("d2a80ac3-6ac6-4e10-b471-e292d65ca369");

    public static BikerAggregate bikerAggregate() {
        return BikerAggregate.builder()
                .withId(BIKER_ID)
                .withUserName("biker_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withEmail("tomasz@123.pl")
                .withIsActive(Boolean.TRUE)
                .withPhoneNumber("123-456-789")
                .withDateOfBirth(LocalDate.of(1989, 2, 23))
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static BikerAggregate disableBikerAggregate() {
        return BikerAggregate.builder()
                .withId(BIKER_ID)
                .withUserName("biker_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withEmail("tomasz@123.pl")
                .withIsActive(Boolean.FALSE)
                .withPhoneNumber("123-456-789")
                .withDateOfBirth(LocalDate.of(1989, 2, 23))
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static BikerCreateData bikerCreateData() {
        return BikerCreateData.builder()
                .withId(BIKER_ID)
                .withUserName("biker_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withEmail("tomasz@123.pl")
                .withEnable(Boolean.TRUE)
                .withPhoneNumber("123-456-789")
                .withDateOfBirth(LocalDate.of(1989, 2, 23))
                .build();
    }

    public static BikerUpdateData bikerUpdateData() {
        return BikerUpdateData.builder()
                .withId(BIKER_ID)
                .withUserName("biker_123")
                .withFirstName("Patryk")
                .withLastName("Patrykowski")
                .withEmail("patryk@123.pl")
                .withIsActive(Boolean.TRUE)
                .withPhoneNumber("465-039-375")
                .withDateOfBirth(LocalDate.of(1989, 2, 23))
                .build();
    }

    public static MotorcycleAggregate motorcycleAggregate() {
        return MotorcycleAggregate.builder()
                .withId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2022)
                .withSerialNumber("IBUWFCE782BJKCWE")
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withIsActive(Boolean.TRUE)
                .build();
    }

    public static MotorcycleCreateData motorcycleCreateData() {
        return MotorcycleCreateData.builder()
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2022)
                .withSerialNumber("IBUWFCE782BJKCWE")
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT.toString())
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static MotorcycleMapEngineData motorcycleMapEngineData() {
        return MotorcycleMapEngineData.builder()
                .withHorsePower(235)
                .withCapacity(1300)
                .build();
    }

    public static MotorcycleUpdateOwnerData motorcycleOwnerUpdateData() {
        return new MotorcycleUpdateOwnerData(RECIPIENT_ID);
    }

}