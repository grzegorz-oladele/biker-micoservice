package pl.grzegorz.bikerdomain.biker.exception;

public class BikerUpdateException extends DomainException {

    public BikerUpdateException(String message) {
        super(message);
    }
}