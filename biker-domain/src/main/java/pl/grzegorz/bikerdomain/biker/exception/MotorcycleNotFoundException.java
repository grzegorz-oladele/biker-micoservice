package pl.grzegorz.bikerdomain.biker.exception;

public class MotorcycleNotFoundException extends DomainException {
    public MotorcycleNotFoundException(String message) {
        super(message);
    }
}