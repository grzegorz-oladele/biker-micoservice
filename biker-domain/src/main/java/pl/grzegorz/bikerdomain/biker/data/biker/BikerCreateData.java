package pl.grzegorz.bikerdomain.biker.data.biker;

import lombok.Builder;

import java.time.LocalDate;
import java.util.UUID;

@Builder(toBuilder = true, setterPrefix = "with")
public record BikerCreateData(

        UUID id,
        String firstName,
        String lastName,
        String userName,
        String email,
        Boolean enable,
        String phoneNumber,
        LocalDate dateOfBirth
) {
}
