package pl.grzegorz.bikerdomain.biker.data.motorcycle;

import lombok.Builder;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder(setterPrefix = "with")
public record MotorcycleUpdateData(
        UUID id,
        UUID bikerId,
        String brand,
        String model,
        int capacity,
        int horsePower,
        int vintage,
        boolean isActive,
        String serialNumber,
        MotorcycleClass motorcycleClass,
        LocalDateTime createdAt,
        LocalDateTime modifiedAt
) {
}