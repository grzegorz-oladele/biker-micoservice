package pl.grzegorz.bikerdomain.biker.aggregates;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import pl.grzegorz.bikerdomain.biker.data.biker.BikerCreateData;
import pl.grzegorz.bikerdomain.biker.data.biker.BikerUpdateData;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Accessors(fluent = true)
@Builder(toBuilder = true, setterPrefix = "with")
public class BikerAggregate {

    private UUID id;
    private String firstName;
    private String lastName;
    private String userName;
    private String email;
    private String phoneNumber;
    private LocalDate dateOfBirth;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;
    private Boolean isActive;

    public static BikerAggregate create(BikerCreateData createData) {
        return BikerAggregate.builder()
                .withId(createData.id())
                .withFirstName(createData.firstName())
                .withLastName(createData.lastName())
                .withUserName(createData.userName())
                .withEmail(createData.email())
                .withPhoneNumber(createData.phoneNumber())
                .withDateOfBirth(createData.dateOfBirth())
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withIsActive(createData.enable())
                .build();
    }

    public static BikerAggregate update(BikerUpdateData updateData) {
        return BikerAggregate.builder()
                .withId(updateData.id())
                .withFirstName(updateData.firstName())
                .withLastName(updateData.lastName())
                .withUserName(updateData.userName())
                .withEmail(updateData.email())
                .withPhoneNumber(updateData.phoneNumber())
                .withDateOfBirth(updateData.dateOfBirth())
                .withCreatedAt(updateData.createdAt())
                .withModifiedAt(updateData.modifiedAt())
                .withIsActive(updateData.isActive())
                .build();
    }

    public void enable() {
        isActive = Boolean.TRUE;
        modifiedAt = LocalDateTime.now();
    }

    public void disable() {
        isActive = Boolean.FALSE;
        modifiedAt = LocalDateTime.now();
    }
}