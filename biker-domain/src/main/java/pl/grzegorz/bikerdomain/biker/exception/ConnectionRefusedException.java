package pl.grzegorz.bikerdomain.biker.exception;

public class ConnectionRefusedException extends DomainException {

    public ConnectionRefusedException(String message) {
        super(message);
    }
}