package pl.grzegorz.bikerdomain.biker.exception;

public class BikerPermissionException extends DomainException {

    public BikerPermissionException(String message) {
        super(message);
    }
}