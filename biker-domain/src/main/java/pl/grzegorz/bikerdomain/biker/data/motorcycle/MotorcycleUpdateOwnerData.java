package pl.grzegorz.bikerdomain.biker.data.motorcycle;

import java.util.UUID;

public record MotorcycleUpdateOwnerData(
        UUID newMotorcycleOwnerId
) {
}