package pl.grzegorz.bikerdomain.biker.data.biker;

import lombok.Builder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Builder(toBuilder = true, setterPrefix = "with")
public record BikerUpdateData(

        UUID id,
        String firstName,
        String lastName,
        String userName,
        String email,
        String phoneNumber,
        LocalDate dateOfBirth,
        LocalDateTime createdAt,
        LocalDateTime modifiedAt,
        Boolean isActive
) {
}
