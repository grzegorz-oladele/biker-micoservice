package pl.grzegorz.bikerdomain.biker.exception;

public class BikerNotFoundException extends DomainException {

    public BikerNotFoundException(String message) {
        super(message);
    }
}