package pl.grzegorz.bikerdomain.biker.exception.messages;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@Getter
@Accessors(fluent = true)
@RequiredArgsConstructor
public enum ExceptionMessage {

    BIKER_NOT_FOUND_MESSAGE("Biker with bikerId -> [%s] not found"),
    BIKER_PERMISSION_EXCEPTION("Biker using id -> [%s] hasn't permission to motorcycle using id -> [%s]"),
    MOTORCYCLE_NOT_FOUND_MESSAGE("Motorcycle using id -> [%s] not found");

    private final String message;

}