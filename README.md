# biker-microservice



## Description
The biker-microservice is responsible for aggregating physical users (bikers) of the application. It allows adding, 
aggregating, filtering, and displaying details of bikers. It closely collaborates with the motorcycle-microservice 
and retrieves information about the motorcycles owned by a particular user when invoking the relevant endpoint

## Technologies Used
- [ ] Spring Boot 3.x.x
- [ ] Hibernate
- [ ] PostgreSQL
- [ ] JUnit
- [ ] Testcontainers
- [ ] RabbitMQ

## Example Usage
- [ ] Endpoint: <span style="color:green">GET</span> /bikers?page=0&size=10
    - Description: Retrieve details of bikers in the application.
    - Response: Returns a list of bikers with their details.
- [ ] Endpoint: <span style="color:green">GET</span> /bikers/`{bikerId}`
    - Description: Retrieve details of a specific biker based on the ID
    - Parameters: `{bikerId}` - The ID of the biker.
    - Response: Returns the details of the specified biker, including their associated motorcycles.
- [ ] Endpoint: <span style="color:green">GET</span> /bikers/`{bikerId}`/exists
    - Description: Add a new biker to the application.
    - Parameters: `{bikerId}` - The ID of the biker.
    - Response: Returns information on whether the motorcyclist exists in the database
- [ ] Endpoint: <span style="color:orange">POST</span>  /bikers
    - Description: Add new biker to the application
    - Request Body: Provide the necessary biker information
    - Response: Returns the ID of the newly created biker
- [ ] Endpoint: <span style="color:purple">PATCH</span> /bikers/`{bikerId}`
    - Description: Update biker information
    - Parameters: `{bikerId}` - The ID of the biker
    - Request Body: Provide the necessary biker information
- [ ] Endpoint: <span style="color:royalblue">PUT</span> /bikers/`{bikerId}`/toggle-active
    - Description: Change of motorcyclist's activity status in the application to the opposite one
    - Parameters: `{bikerId}` - The ID of the biker

## Architecture
The project follows Domain-Driven Design (DDD) and event storming principles. It consists of four modules:
- [ ] **Server**: Responsible for application startup, integration tests, and management of environment variables
- [ ] **Domain**: The core module that contains key objects and dedicated methods for working with those objects
- [ ] **Application**: Exposes application use cases
- [ ] **Adapters**: Handles communication with external systems and other microservices within the cluster

## Custom Pipeline
This repository includes a custom pipeline that automates the build, testing, and deployment process for the application
The pipeline is designed to perform the following steps:

1. Build and test:
  - [ ] The pipeline triggers a build process using Maven to compile and package the application.
  - [ ] It runs the tests to ensure the code quality and functionality.
  - [ ] If the build and tests pass successfully, the pipeline proceeds to the next step.
  - [ ] In case the testing process fails, the pipeline terminates, and the test logs are saved in the newly created
    **errors.txt** file

2. Commit and push:
  - [ ] The pipeline commits and pushes the changes to the Git repository on GitLab.
  - [ ] It includes the commit message provided as an argument when running the pipeline.

3. Docker image build and push:
  - [ ] The pipeline builds a Docker image of the application.
  - [ ] It tags the Docker image with the first 6 characters of the current commit's hash.
  - [ ] If the optional -d (docker) argument is present when running the pipeline, it automatically pushes the Docker
    image to the Docker Hub repository.

***Example command execution:*** `biker-build -cm"commit message" -d`

## Project Status
The microservice for motorcyclists is being actively developed. We are constantly working on adding new features including:
- [ ] implementation of the CQRS pattern
- [ ] addition and implementation of Liquibase library
- [ ] implementation of security using the Spring Security framework
- [ ] increasing test coverage of key application functionalities

# Author
**Grzegorz Oladele**

Thank you for your interest in the biker-microservice project! If you have any questions or feedback, please
feel free to contact us.
