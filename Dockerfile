FROM amazoncorretto:17-alpine
EXPOSE 8100
WORKDIR /app
COPY biker-service-server/src/main/resources/db /app
COPY biker-service-server/target/biker-service-server-0.0.1-SNAPSHOT.jar /app/biker-service-server-0.0.1-SNAPSHOT.jar
CMD ["java", "-jar", "biker-service-server-0.0.1-SNAPSHOT.jar"]