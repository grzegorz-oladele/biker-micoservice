package pl.grzegorz.bikerapplication;

import lombok.NoArgsConstructor;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleMapEngineCommandUseCase.MotorcycleMapEngineCommand;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;
import pl.grzegorz.bikerdomain.biker.data.motorcycle.MotorcycleClass;

import java.util.UUID;

@NoArgsConstructor
public class MotorcycleFixtures {

    private static final UUID MOTORCYCLE_ID = UUID.fromString("aa1e258f-40d5-432e-ae45-7dc6ac92bd02");
    private static final UUID BIKER_ID = UUID.fromString("3606fe74-db53-4007-b45f-c735a8054d4b");
    private static final UUID SECOND_BIKER_ID = UUID.fromString("62735389-6b89-4039-89ff-90f2e486704b");

    public static MotorcycleAggregate motorcycleAggregate() {
        return MotorcycleAggregate.builder()
                .withId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2022)
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .withIsActive(Boolean.TRUE)
                .withSerialNumber("BUIWE8734BIWVC")
                .build();
    }

    public static MotorcycleMapEngineCommand motorcycleMapEngineCommand() {
        return MotorcycleMapEngineCommand.builder()
                .withId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withCapacity(599)
                .withHorsePower(123)
                .build();
    }

    public static MotorcycleOwnerUpdateCommandUseCase.MotorcycleOwnerUpdateCommand motorcycleOwnerUpdateCommand() {
        return MotorcycleOwnerUpdateCommandUseCase.MotorcycleOwnerUpdateCommand.builder()
                .withMotorcycleId(MOTORCYCLE_ID)
                .withOwnerTransferorId(BIKER_ID)
                .withNewOwnerId(SECOND_BIKER_ID)
                .build();
    }
}
