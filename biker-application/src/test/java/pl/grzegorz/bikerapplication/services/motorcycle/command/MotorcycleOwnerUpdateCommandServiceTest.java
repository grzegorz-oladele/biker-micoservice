package pl.grzegorz.bikerapplication.services.motorcycle.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase.MotorcycleOwnerUpdateCommand;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.command.MotorcycleOwnerUpdateCommandPort;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.bikerapplication.MotorcycleFixtures.motorcycleAggregate;
import static pl.grzegorz.bikerapplication.MotorcycleFixtures.motorcycleOwnerUpdateCommand;

@ExtendWith(MockitoExtension.class)
class MotorcycleOwnerUpdateCommandServiceTest {

    @InjectMocks
    private MotorcycleOwnerUpdateCommandService motorcycleOwnerUpdateCommandService;
    @Mock
    private MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    @Mock
    private MotorcycleOwnerUpdateCommandPort motorcycleOwnerUpdateCommandPort;

    private MotorcycleOwnerUpdateCommand motorcycleOwnerUpdateCommand;
    private MotorcycleAggregate motorcycleAggregate;

    @BeforeEach
    void setup() {
        motorcycleOwnerUpdateCommand = motorcycleOwnerUpdateCommand();
        motorcycleAggregate = motorcycleAggregate();
    }

    @Test
    void shouldCallUpdateOwnerOnMotorcycleOwnerUpdateCommandPortInterface() {
        var motorcycleId = motorcycleOwnerUpdateCommand.motorcycleId().toString();
        var ownerId = motorcycleOwnerUpdateCommand.ownerTransferorId().toString();
//        given
        when(motorcycleByIdQueryPort.getById(motorcycleId, ownerId)).thenReturn(motorcycleAggregate);
//        when
        motorcycleOwnerUpdateCommandService.update(motorcycleOwnerUpdateCommand);
//        then
        verify(motorcycleOwnerUpdateCommandPort).updateOwner(motorcycleAggregate);
    }
}