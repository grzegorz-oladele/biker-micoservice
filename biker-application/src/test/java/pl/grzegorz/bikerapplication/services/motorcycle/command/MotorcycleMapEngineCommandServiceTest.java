package pl.grzegorz.bikerapplication.services.motorcycle.command;

import net.bytebuddy.implementation.FixedValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.bikerapplication.MotorcycleFixtures;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleMapEngineCommandUseCase.MotorcycleMapEngineCommand;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.command.MotorcycleMapEngineCommandPort;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MotorcycleMapEngineCommandServiceTest {

    @InjectMocks
    private MotorcycleMapEngineCommandService motorcycleMapEngineCommandService;
    @Mock
    private MotorcycleMapEngineCommandPort motorcycleMapEngineCommandPort;
    @Mock
    private MotorcycleByIdQueryPort motorcycleByIdQueryPort;

    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleMapEngineCommand motorcycleMapEngineCommand;

    @BeforeEach
    void setup() {
        motorcycleAggregate = MotorcycleFixtures.motorcycleAggregate();
        motorcycleMapEngineCommand = MotorcycleFixtures.motorcycleMapEngineCommand();
    }

    @Test
    void shouldCallMapMethodOnMotorcycleMapEngineCommandPortInterface() {
//        given
        var motorcycleId = motorcycleAggregate.id().toString();
        var bikerId = motorcycleAggregate.bikerId().toString();
        when(motorcycleByIdQueryPort.getById(motorcycleId, bikerId)).thenReturn(motorcycleAggregate);
//        when
        motorcycleMapEngineCommandService.map(motorcycleMapEngineCommand);
//        then
        verify(motorcycleMapEngineCommandPort).map(motorcycleAggregate);
    }
}