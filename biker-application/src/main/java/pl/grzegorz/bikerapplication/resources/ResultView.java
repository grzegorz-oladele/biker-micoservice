package pl.grzegorz.bikerapplication.resources;

import lombok.Builder;

import java.util.List;

@Builder(toBuilder = true, setterPrefix = "with")
public record ResultView<T>(
        PageInfo pageInfo,
        List<T> results
) {
}