package pl.grzegorz.bikerapplication.ports.out.motorcycle.command;

import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

public interface MotorcycleMapEngineCommandPort {

    void map(MotorcycleAggregate motorcycleAggregate);
}