package pl.grzegorz.bikerapplication.ports.view;

import lombok.Builder;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Builder(toBuilder = true, setterPrefix = "with")
public record BikerView(
        UUID id,
        String firstName,
        String userName,
        String email,
        String phoneNumber,
        LocalDate dateOfBirth,
        LocalDateTime createdAt,
        LocalDateTime modifiedAt,
        Boolean isActive,
        List<MotorcycleView> motorcycles
) {

    public static BikerView toView(BikerAggregate aggregate) {
        return BikerView.builder()
                .withId(aggregate.id())
                .withFirstName(aggregate.firstName())
                .withUserName(aggregate.userName())
                .withPhoneNumber(aggregate.phoneNumber())
                .withEmail(aggregate.email())
                .withDateOfBirth(aggregate.dateOfBirth())
                .withCreatedAt(aggregate.createdAt())
                .withModifiedAt(aggregate.modifiedAt())
                .withIsActive(aggregate.isActive())
                .build();
    }
}