package pl.grzegorz.bikerapplication.ports.view;

import lombok.Builder;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

import java.util.UUID;

@Builder(setterPrefix = "with")
public record MotorcycleView(
        UUID motorcycleId,
        String brand,
        String model,
        int capacity,
        int horsePower,
        int vintage,
        String serialNumber
) {

    public static MotorcycleView toView(MotorcycleAggregate motorcycleAggregate) {
        return MotorcycleView.builder()
                .withMotorcycleId(motorcycleAggregate.id())
                .withBrand(motorcycleAggregate.brand())
                .withModel(motorcycleAggregate.model())
                .withCapacity(motorcycleAggregate.capacity())
                .withHorsePower(motorcycleAggregate.horsePower())
                .withVintage(motorcycleAggregate.vintage())
                .withSerialNumber(motorcycleAggregate.serialNumber())
                .build();
    }
}