package pl.grzegorz.bikerapplication.ports.in.motorcycle.command;

import lombok.Builder;
import pl.grzegorz.bikerdomain.biker.data.motorcycle.MotorcycleUpdateOwnerData;

import java.util.UUID;

public interface MotorcycleOwnerUpdateCommandUseCase {

    void update(MotorcycleOwnerUpdateCommand motorcycleOwnerUpdateCommand);

    @Builder(setterPrefix = "with")
    record MotorcycleOwnerUpdateCommand(
            UUID motorcycleId,
            UUID ownerTransferorId,
            UUID newOwnerId
    ) {

        public MotorcycleUpdateOwnerData toUpdateOwnerData() {
            return new MotorcycleUpdateOwnerData(newOwnerId);
        }
    }
}