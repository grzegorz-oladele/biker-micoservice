package pl.grzegorz.bikerapplication.ports.out.motorcycle.command;

import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

public interface MotorcycleSetUnactiveCommandPort {

    void setUnactive(MotorcycleAggregate motorcycleAggregate, String bikerId);
}