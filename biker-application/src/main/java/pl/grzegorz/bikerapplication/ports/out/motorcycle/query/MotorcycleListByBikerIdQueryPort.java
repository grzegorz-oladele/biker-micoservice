package pl.grzegorz.bikerapplication.ports.out.motorcycle.query;

import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

import java.util.List;

public interface MotorcycleListByBikerIdQueryPort {

    List<MotorcycleAggregate> getMotorcycleListByBikerId(String bikerId);
}