package pl.grzegorz.bikerapplication.ports.out.biker.query;

import pl.grzegorz.bikerapplication.resources.ResourceFilter;
import pl.grzegorz.bikerapplication.resources.ResultView;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

public interface BikerPageQueryPort {

  ResultView<BikerAggregate> getBikers(ResourceFilter filter);
}