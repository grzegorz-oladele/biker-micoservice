package pl.grzegorz.bikerapplication.ports.in.biker.query;

import pl.grzegorz.bikerapplication.ports.view.BikerView;

import java.util.UUID;

public interface BikerByIdQueryUseCase {

    BikerView execute(UUID id);
}