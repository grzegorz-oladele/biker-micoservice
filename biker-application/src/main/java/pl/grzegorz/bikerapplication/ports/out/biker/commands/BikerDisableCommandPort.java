package pl.grzegorz.bikerapplication.ports.out.biker.commands;

import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

public interface BikerDisableCommandPort {

    void disable(BikerAggregate bikerAggregate);
}