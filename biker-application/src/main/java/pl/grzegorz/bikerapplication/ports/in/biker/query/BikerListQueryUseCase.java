package pl.grzegorz.bikerapplication.ports.in.biker.query;

import pl.grzegorz.bikerapplication.ports.view.BikerView;
import pl.grzegorz.bikerapplication.resources.ResourceFilter;
import pl.grzegorz.bikerapplication.resources.ResultView;

public interface BikerListQueryUseCase {

    ResultView<BikerView> execute(ResourceFilter filter);
}