package pl.grzegorz.bikerapplication.ports.out.biker.query;

import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

public interface BikerSimplifiedByIdQueryPort {

    BikerAggregate getBikerSimplifiedById(String bikerId);
}