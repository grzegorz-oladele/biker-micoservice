package pl.grzegorz.bikerapplication.ports.in.biker.commands;

public interface BikerDisableCommandUseCase {

    void disable(String bikerId);
}