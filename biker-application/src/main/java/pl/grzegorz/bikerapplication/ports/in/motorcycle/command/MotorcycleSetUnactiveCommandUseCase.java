package pl.grzegorz.bikerapplication.ports.in.motorcycle.command;

public interface MotorcycleSetUnactiveCommandUseCase {

    void setUnactive(String motorcycleId, String bikerId);
}