package pl.grzegorz.bikerapplication.ports.in.biker.commands;

import lombok.Builder;
import pl.grzegorz.bikerdomain.biker.data.biker.BikerCreateData;

import java.time.LocalDate;
import java.util.UUID;

public interface BikerCreateCommandUseCase {

    UUID execute(BikerCreateCommand command);

    @Builder(toBuilder = true, setterPrefix = "with")
    record BikerCreateCommand(
            UUID id,
            String firstName,
            String lastName,
            String userName,
            String email,
            Boolean enable,
            String phoneNumber,
            String dateOfBirth
    ) {
        public BikerCreateData toCreateData() {
            return BikerCreateData.builder()
                    .withId(id)
                    .withFirstName(firstName)
                    .withLastName(lastName)
                    .withUserName(userName)
                    .withEmail(email)
                    .withEnable(enable)
                    .withPhoneNumber(phoneNumber)
                    .withDateOfBirth(LocalDate.parse(dateOfBirth))
                    .build();
        }
    }
}