package pl.grzegorz.bikerapplication.ports.in.motorcycle.command;

import lombok.Builder;
import pl.grzegorz.bikerdomain.biker.data.motorcycle.MotorcycleMapEngineData;

import java.util.UUID;

public interface MotorcycleMapEngineCommandUseCase {

    void map(MotorcycleMapEngineCommand mapEngineCommand);

    @Builder(setterPrefix = "with")
    record MotorcycleMapEngineCommand(
            UUID id,
            UUID bikerId,
            int capacity,
            int horsePower
    ) {
        public MotorcycleMapEngineData toMapEngineData() {
            return MotorcycleMapEngineData.builder()
                    .withCapacity(capacity)
                    .withHorsePower(horsePower)
                    .build();
        }
    }
}