package pl.grzegorz.bikerapplication.ports.in.motorcycle.command;

import lombok.Builder;
import pl.grzegorz.bikerdomain.biker.data.motorcycle.MotorcycleClass;
import pl.grzegorz.bikerdomain.biker.data.motorcycle.MotorcycleCreateData;

import java.time.LocalDateTime;
import java.util.UUID;

public interface MotorcycleCreateCommandUseCase {

    void create(MotorcycleCreateCommand command);

    @Builder(setterPrefix = "with")
    record MotorcycleCreateCommand(
            UUID id,
            UUID bikerId,
            String brand,
            String model,
            int capacity,
            int horsePower,
            int vintage,
            MotorcycleClass motorcycleClass,
            String serialNumber
    ) {
        public MotorcycleCreateData toCreateData() {
            return MotorcycleCreateData.builder()
                    .withId(id)
                    .withBikerId(bikerId)
                    .withBrand(brand)
                    .withModel(model)
                    .withCapacity(capacity)
                    .withHorsePower(horsePower)
                    .withVintage(vintage)
                    .withMotorcycleClass(motorcycleClass.toString())
                    .withSerialNumber(serialNumber)
                    .withCreatedAt(LocalDateTime.now())
                    .withModifiedAt(LocalDateTime.now())
                    .build();
        }
    }
}