package pl.grzegorz.bikerapplication.ports.in.biker.commands;

public interface BikerEnableCommandUseCase {

    void enable(String bikerId);
}