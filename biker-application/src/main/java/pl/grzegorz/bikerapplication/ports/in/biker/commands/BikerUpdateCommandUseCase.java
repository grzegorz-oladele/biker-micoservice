package pl.grzegorz.bikerapplication.ports.in.biker.commands;

import lombok.Builder;
import pl.grzegorz.bikerapplication.vo.ConstBikerUpdateValue;
import pl.grzegorz.bikerdomain.biker.data.biker.BikerUpdateData;

import java.time.LocalDateTime;
import java.util.UUID;

public interface BikerUpdateCommandUseCase {

    void execute(BikerUpdateCommand command);

    @Builder(toBuilder = true, setterPrefix = "with")
    record BikerUpdateCommand(
            UUID id,
            String firstName,
            String lastName,
            String userName,
            String email,
            String phoneNumber,
            LocalDateTime modifiedAt
    ) {
        public BikerUpdateData toUpdateData(ConstBikerUpdateValue constUpdateValue) {
            return BikerUpdateData.builder()
                    .withId(constUpdateValue.bikerId())
                    .withFirstName(firstName)
                    .withLastName(lastName)
                    .withEmail(email)
                    .withUserName(constUpdateValue.username())
                    .withPhoneNumber(phoneNumber)
                    .withDateOfBirth(constUpdateValue.dateOfBirth())
                    .withCreatedAt(constUpdateValue.createdAt())
                    .withModifiedAt(LocalDateTime.now())
                    .withIsActive(constUpdateValue.isActive())
                    .build();
        }
    }
}