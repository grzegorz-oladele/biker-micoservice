package pl.grzegorz.bikerapplication.ports.out.biker.commands;

import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

public interface BikerCreateCommandPort {

    void execute(BikerAggregate aggregate);
}