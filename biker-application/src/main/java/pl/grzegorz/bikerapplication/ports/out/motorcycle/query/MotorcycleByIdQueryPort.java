package pl.grzegorz.bikerapplication.ports.out.motorcycle.query;

import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

public interface MotorcycleByIdQueryPort {

    MotorcycleAggregate getById(String motorcycleId, String bikerId);
}