package pl.grzegorz.bikerapplication.ports.out.biker.query;

public interface BikerExistsByIdQueryPort {

    Boolean existsById(String bikerId);
}