package pl.grzegorz.bikerapplication.vo;

import lombok.AccessLevel;
import lombok.Builder;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Builder(toBuilder = true, setterPrefix = "with", access = AccessLevel.PRIVATE)
public record ConstBikerUpdateValue(
        UUID bikerId,
        LocalDateTime createdAt,
        String username,
        Boolean isActive,
        LocalDate dateOfBirth

) {

    public static ConstBikerUpdateValue toConstBikerUpdateValue(BikerAggregate aggregate) {
        return ConstBikerUpdateValue.builder()
                .withBikerId(aggregate.id())
                .withCreatedAt(aggregate.createdAt())
                .withIsActive(aggregate.isActive())
                .withUsername(aggregate.userName())
                .withDateOfBirth(aggregate.dateOfBirth())
                .build();
    }
}