package pl.grzegorz.bikerapplication.services.motorcycle.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleSetUnactiveCommandUseCase;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.command.MotorcycleSetUnactiveCommandPort;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;

@RequiredArgsConstructor
public class MotorcycleSetUnactiveCommandService implements MotorcycleSetUnactiveCommandUseCase {

    private final MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    private final MotorcycleSetUnactiveCommandPort motorcycleSetUnactiveCommandPort;

    @Override
    public void setUnactive(String motorcycleId, String bikerId) {
        var motorcycleAggregate = motorcycleByIdQueryPort.getById(motorcycleId, bikerId);
        motorcycleSetUnactiveCommandPort.setUnactive(motorcycleAggregate, bikerId);
    }
}