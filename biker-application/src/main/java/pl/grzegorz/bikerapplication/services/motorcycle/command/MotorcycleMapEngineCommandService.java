package pl.grzegorz.bikerapplication.services.motorcycle.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleMapEngineCommandUseCase;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.command.MotorcycleMapEngineCommandPort;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;
import pl.grzegorz.bikerdomain.biker.exception.BikerPermissionException;
import pl.grzegorz.bikerdomain.biker.exception.messages.ExceptionMessage;

@RequiredArgsConstructor
public class MotorcycleMapEngineCommandService implements MotorcycleMapEngineCommandUseCase {

    private final MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    private final MotorcycleMapEngineCommandPort motorcycleMapEngineCommandPort;

    @Override
    public void map(MotorcycleMapEngineCommand mapEngineCommand) {
        var motorcycleId = mapEngineCommand.id().toString();
        var bikerId = mapEngineCommand.bikerId().toString();
        var motorcycleAggregate = motorcycleByIdQueryPort.getById(motorcycleId, bikerId);
        validateBikerPermission(motorcycleAggregate, bikerId);
        motorcycleAggregate.mapEngine(mapEngineCommand.toMapEngineData());
        motorcycleMapEngineCommandPort.map(motorcycleAggregate);
    }

    private void validateBikerPermission(MotorcycleAggregate motorcycleAggregate, String bikerId) {
        if (!motorcycleAggregate.bikerId().toString().equals(bikerId)) {
            throw new BikerPermissionException(String.format(
                    ExceptionMessage.BIKER_PERMISSION_EXCEPTION.message(), bikerId, motorcycleAggregate.id()));
        }
    }
}