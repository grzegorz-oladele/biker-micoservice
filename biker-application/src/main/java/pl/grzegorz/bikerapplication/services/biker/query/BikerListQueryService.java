package pl.grzegorz.bikerapplication.services.biker.query;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.bikerapplication.ports.in.biker.query.BikerListQueryUseCase;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerPageQueryPort;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.query.MotorcycleListByBikerIdQueryPort;
import pl.grzegorz.bikerapplication.ports.view.BikerView;
import pl.grzegorz.bikerapplication.ports.view.MotorcycleView;
import pl.grzegorz.bikerapplication.resources.ResourceFilter;
import pl.grzegorz.bikerapplication.resources.ResultView;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class BikerListQueryService implements BikerListQueryUseCase {

    private final BikerPageQueryPort bikerPageQueryPort;
    private final MotorcycleListByBikerIdQueryPort motorcycleListByBikerIdQueryPort;

    @Override
    public ResultView<BikerView> execute(ResourceFilter filter) {
        var bikerAggregatesPage = bikerPageQueryPort.getBikers(filter);
        return ResultView.<BikerView>builder()
                .withPageInfo(bikerAggregatesPage.pageInfo())
                .withResults(bikerAggregatesPage.results().stream()
                        .map(biker -> BikerView.toView(biker).toBuilder()
                                .withMotorcycles(motorcycles(biker.id().toString()))
                                .build())
                        .collect(Collectors.toList()))
                .build();
    }

    private List<MotorcycleView> motorcycles(String bikerId) {
        return motorcycleListByBikerIdQueryPort.getMotorcycleListByBikerId(bikerId).stream()
                .map(MotorcycleView::toView)
                .collect(Collectors.toList());
    }
}