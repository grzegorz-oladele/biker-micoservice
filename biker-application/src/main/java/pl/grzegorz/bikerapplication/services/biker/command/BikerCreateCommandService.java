package pl.grzegorz.bikerapplication.services.biker.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.bikerapplication.ports.in.biker.commands.BikerCreateCommandUseCase;
import pl.grzegorz.bikerapplication.ports.out.biker.commands.BikerCreateCommandPort;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

import java.util.UUID;

@RequiredArgsConstructor
public class BikerCreateCommandService implements BikerCreateCommandUseCase {

    private final BikerCreateCommandPort bikerCreateCommandPort;

    @Override
    public UUID execute(BikerCreateCommand command) {
        var bikerAggregate = BikerAggregate.create(command.toCreateData());
        bikerCreateCommandPort.execute(bikerAggregate);
        return bikerAggregate.id();
    }
}