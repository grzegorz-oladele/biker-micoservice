package pl.grzegorz.bikerapplication.services.motorcycle.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.command.MotorcycleOwnerUpdateCommandPort;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;

@RequiredArgsConstructor
public class MotorcycleOwnerUpdateCommandService implements MotorcycleOwnerUpdateCommandUseCase {

    private final MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    private final MotorcycleOwnerUpdateCommandPort motorcycleOwnerUpdateCommandPort;

    @Override
    public void update(MotorcycleOwnerUpdateCommand ownerUpdateCommand) {
        var motorcycleAggregate = motorcycleByIdQueryPort.getById(ownerUpdateCommand.motorcycleId().toString(),
                ownerUpdateCommand.ownerTransferorId().toString());
        motorcycleAggregate.updateOwner(ownerUpdateCommand.toUpdateOwnerData());
        motorcycleOwnerUpdateCommandPort.updateOwner(motorcycleAggregate);
    }
}
