package pl.grzegorz.bikerapplication.services.biker.query;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.bikerapplication.ports.in.biker.query.BikerByIdQueryUseCase;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.query.MotorcycleListByBikerIdQueryPort;
import pl.grzegorz.bikerapplication.ports.view.BikerView;
import pl.grzegorz.bikerapplication.ports.view.MotorcycleView;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class BikerByIdQueryService implements BikerByIdQueryUseCase {

    private final BikerByIdQueryPort bikerByIdQueryPort;
    private final MotorcycleListByBikerIdQueryPort motorcycleListByBikerIdQueryPort;

    @Override
    public BikerView execute(UUID id) {
        var aggregate = bikerByIdQueryPort.getById(id.toString());
        return BikerView.toView(aggregate).toBuilder()
                .withMotorcycles(motorcycles(id.toString()))
                .build();
    }

    private List<MotorcycleView> motorcycles(String bikerId) {
        return motorcycleListByBikerIdQueryPort.getMotorcycleListByBikerId(bikerId).stream()
                .map(MotorcycleView::toView)
                .collect(Collectors.toList());
    }
}