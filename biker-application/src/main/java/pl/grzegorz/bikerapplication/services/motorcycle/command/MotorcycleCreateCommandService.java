package pl.grzegorz.bikerapplication.services.motorcycle.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.bikerapplication.ports.in.motorcycle.command.MotorcycleCreateCommandUseCase;
import pl.grzegorz.bikerapplication.ports.out.motorcycle.command.MotorcycleCreateCommandPort;
import pl.grzegorz.bikerdomain.biker.aggregates.MotorcycleAggregate;

@RequiredArgsConstructor
public class MotorcycleCreateCommandService implements MotorcycleCreateCommandUseCase {

    private final MotorcycleCreateCommandPort motorcycleCreateCommandPort;

    @Override
    public void create(MotorcycleCreateCommand command) {
        var motorcycleaggregate = MotorcycleAggregate.create(command.toCreateData());
        motorcycleCreateCommandPort.create(motorcycleaggregate);
    }
}