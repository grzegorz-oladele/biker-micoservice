package pl.grzegorz.bikerapplication.services.biker.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.bikerapplication.ports.in.biker.commands.BikerEnableCommandUseCase;
import pl.grzegorz.bikerapplication.ports.out.biker.commands.BikerEnableCommandPort;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerByIdSimplifiedQueryPort;

@RequiredArgsConstructor
public class BikerEnableCommandService implements BikerEnableCommandUseCase {

    private final BikerByIdSimplifiedQueryPort bikerByIdSimplifiedQueryPort;
    private final BikerEnableCommandPort bikerEnableCommandPort;

    @Override
    public void enable(String bikerId) {
        var bikerAggregate = bikerByIdSimplifiedQueryPort.execute(bikerId);
        bikerAggregate.enable();
        bikerEnableCommandPort.enable(bikerAggregate);
    }
}