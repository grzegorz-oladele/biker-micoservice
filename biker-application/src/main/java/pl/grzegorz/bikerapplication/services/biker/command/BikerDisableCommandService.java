package pl.grzegorz.bikerapplication.services.biker.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.bikerapplication.ports.in.biker.commands.BikerDisableCommandUseCase;
import pl.grzegorz.bikerapplication.ports.out.biker.commands.BikerDisableCommandPort;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerByIdSimplifiedQueryPort;

@RequiredArgsConstructor
public class BikerDisableCommandService implements BikerDisableCommandUseCase {

    private final BikerByIdSimplifiedQueryPort bikerByIdSimplifiedQueryPort;
    private final BikerDisableCommandPort bikerDisableCommandPort;

    @Override
    public void disable(String bikerId) {
        var biker = bikerByIdSimplifiedQueryPort.execute(bikerId);
        bikerDisableCommandPort.disable(biker);
    }
}