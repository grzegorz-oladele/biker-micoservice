package pl.grzegorz.bikerapplication.services.biker.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.bikerapplication.ports.in.biker.commands.BikerUpdateCommandUseCase;
import pl.grzegorz.bikerapplication.ports.out.biker.commands.BikerUpdateCommandPort;
import pl.grzegorz.bikerapplication.ports.out.biker.query.BikerByIdToUpdateQueryPort;
import pl.grzegorz.bikerapplication.vo.ConstBikerUpdateValue;
import pl.grzegorz.bikerdomain.biker.aggregates.BikerAggregate;

import static pl.grzegorz.bikerapplication.vo.ConstBikerUpdateValue.toConstBikerUpdateValue;

@RequiredArgsConstructor
public class BikerUpdateCommandService implements BikerUpdateCommandUseCase {

    private final BikerUpdateCommandPort bikerUpdateCommandPort;
    private final BikerByIdToUpdateQueryPort bikerByIdToUpdateQueryPort;

    @Override
    public void execute(BikerUpdateCommand command) {
        ConstBikerUpdateValue constBikerUpdateValue = toConstUpdateValue(command.id().toString());
        var bikerAggregate = BikerAggregate.update(command.toUpdateData(constBikerUpdateValue));
        bikerUpdateCommandPort.update(bikerAggregate);
    }

    private ConstBikerUpdateValue toConstUpdateValue(String bikerId) {
        BikerAggregate bikerAggregate = bikerByIdToUpdateQueryPort.execute(bikerId);
        return toConstBikerUpdateValue(bikerAggregate);
    }
}